<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 10.06.2018
 * Time: 13:40
 */

use app\models\Region;
use yii\helpers\Url;

?>
<section class="personal_area_info">
    <div class="container">
        <div class="personal_area_info_h1top">
            <h1><?= Yii::t('app','Ваша учетная запись на EKROSSOVKI.UZ') ?> - <?= Yii::t('app','Привет') ?> <?= $user->username ?>!</h1>
            <p><?= Yii::t('app','ВЫ НАХОДИТЕСЬ В КАЧЕСТВЕ') ?> <span><?= $user->email ?></span></p>
        </div>
        <div class="personal_area_info-tabs">
            <div class="personal_area_info-tabs_h4">
                <h4><?= Yii::t('app','ВАША ОСНОВНАЯ ИНФОРМАЦИЯ') ?></h4>
            </div>
            <div class="person_area_info-2tabs">


                <div class="right_tabs_down">

                    <ul class="nav nav-tabs mynav-tabs2">
                        <li class="active"><a data-toggle="tab" href="#my2home"><?= Yii::t('app','ЛИЧНЫЕ ДАННЫЕ') ?></a></li>
                        <li><a data-toggle="tab" href="#my2menu1"><?= Yii::t('app','АДРЕС ДОСТАВКИ') ?></a></li>
                    </ul>

                    <div class="tab-content mytab-content2">
                        <div id="my2home" class="tab-pane fade in active">
                            <div class="info-tab_all-1">
                                <div class="info-tab_all-1-frs ">
                                    <div class="info-tab_all-1-topp">
                                        <p>* <?= Yii::t('app','Указывает обязательное поле') ?></p>
                                    </div>
                                    <form action="<?= Url::to(['/site/change-user']) ?>" method="get">
                                    <div class="info-tab_all-1-frs-in">
                                        <div class="info-tab_all-1-frs-in-left">
                                            <div class="info--in-left">
                                                <h6>*  <?= Yii::t('app','ЛОГИН') ?>: </h6>
                                                <input type="text" name="username" value="<?= $user->username ?>" required="required">
                                            </div>
                                            <div class="info--in-left">
                                                <h6>*  <?= Yii::t('app','ФИО') ?>: </h6>
                                                <input type="text" name="name" value="<?= $user_info->name ?>" required="required">
                                            </div>
                                        </div>
                                        <div class="info-tab_all-1-frs-in-right">
                                            <div class="info--in-left">
                                                <h6>* E-MAIL: </h6>
                                                <input type="text" name="email" value="<?= $user->email ?>" required="required">
                                            </div>
                                            <div class="info--in-left">
                                                <h6>* <?= Yii::t('app','ТЕЛ.') ?>.: </h6>
                                                <input type="text" name="phone" value="<?= $user_info->phone ?>" required="required">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="info-tab_all-1-frs_btn">
                                        <button><?= Yii::t('app','сохранить') ?></button>
                                        <a href="<?= Url::to(['/site/change-password', 'id' =>$user->id]) ?>"><?= Yii::t('app','Сменить пароль') ?></a>
                                    </div>
                                    </form>
                                </div>
                                <div class="info-tab_all-1-second_all div-m-active" >
                                    <div class="info-tab_all-1-second">
                                        <div class="info-tab_-second-1">
                                            <div class="info-tab_-second-1-top">
                                                <div class="info-tab_-second_h6-wi">
                                                    <h6><?= Yii::t('app','Логин') ?>:</h6>
                                                </div>
                                                <div class="info-tab_-second_h6-wi2">
                                                    <h6><?= $user->username ?></h6>
                                                </div>
                                            </div>
                                            <div class="info-tab_-second-1-bottom">
                                                <div class="info-tab_-second_h6-wi">
                                                    <h6><?= Yii::t('app','Фио') ?>:</h6>
                                                </div>
                                                <div class="info-tab_-second_h6-wi2">
                                                    <h6><?= $user_info->name;  ?></h6>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="info-tab_-second-2">
                                            <div class="info-tab_-second-2-top">
                                                <div class="info-tab_-second_h6-wi">
                                                    <h6>e-MAIL:</h6>
                                                </div>
                                                <div class="info-tab_-second_h6-wi2">
                                                    <h6><?= $user->email ?></h6>
                                                </div>
                                            </div>
                                            <div class="info-tab_-second-2-bottom">
                                                <div class="info-tab_-second_h6-wi">
                                                    <h6>Тел.:</h6>
                                                </div>
                                                <div class="info-tab_-second_h6-wi2">
                                                    <h6><?=$user_info->phone ?></h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="info-tab_all-1-frs_btn">
                                        <button class="info-_btn-red"><?= Yii::t('app','Редактировать') ?></button>
                                        <a href="<?= Url::to(['/site/change-password', 'id' =>$user->id]) ?>"><?= Yii::t('app','Сменить пароль') ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="my2menu1" class="tab-pane fade">
                            <div class="info_all_second-_wbtn">
                                <div class="info_all_second-all_wbtn div-m-active-2">
                                    <div class="info_all_second-all">
                                        <div class="info_all_second-all-left">
                                            <div class="info__second--left">
                                                <p><?= Yii::t('app','РЕГИОН') ?>: <span>
                                                        <?php if(!empty($user_info->region_id)): ?>
                                                        <?= $user_info->rayon->region->region ?>
                                                        <?php endif; ?>
                                                    </span></p>
                                            </div>
                                            <div class="info__second--left">
                                                <p><?= Yii::t('app','РАЙОН') ?>: <span>
                                                        <?php if(!empty($user_info->region_id)): ?>
                                                            <?= $user_info->rayon->rayon ?>
                                                        <?php endif; ?></span></p>
                                            </div>
                                            <div class="info__second--left">
                                                <p><?= Yii::t('app','ИНДЕКС') ?>: <span><?= $user_info->index ?></span></p>
                                            </div>
                                        </div>
                                        <div class="info_all_second-all-right">
                                            <div class="info__second--left">
                                                <p><?= Yii::t('app','УЛИЦА') ?>: <span><?= $user_info->address ?></span></p>
                                            </div>
                                            <div class="info__second--left">
                                                <p><?= Yii::t('app','НОМЕР ДОМА') ?>: <span><?= $user_info->number_dom ?></span></p>
                                            </div>
                                            <div class="info__second--left">
                                                <p><?= Yii::t('app','номер квартиры') ?>: <span><?= $user_info->number_kv ?></span></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="info-tab_all-1-frs_btn">
                                        <button class="info-_btn-red2"><?= Yii::t('app','редактировать') ?></button>
                                    </div>
                                </div>
                                <div class="info_all_second-all_wbtn2">

                                    <div class="info-tab_all-1-topp">
                                        <p>* <?= Yii::t('app','Указывает обязательное поле') ?></p>
                                    </div>
                                    <form action="<?= Url::to(['/site/change-address']) ?>" method="get">
                                    <div class="info-tab_all-1-topp-break">
                                        <div class="info_all_second-input-left">
                                            <div class="info_all_second-input-left-1">
                                                <p>* <?= Yii::t('app','РЕГИОН')?>:  </p>
                                                <div class="info_all_second-select-left-1">
                                                    <select name="region" class="region" required="required"  >
                                                        <option disabled selected><?= Yii::t('app','Выберите Регион') ?></option>
                                                        <?php $region = Region::find()->all(); ?>
                                                        <?php foreach ($region as $item): ?>
                                                        <option value="<?= $item->id ?>"><?= $item->region ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                    <span>
	                                            	<i class="fa fa-sort-desc" aria-hidden="true"></i>
	                                            </span>
                                                </div>
                                            </div>
                                            <div class="info_all_second-input-left-1">
                                                <p>* <?= Yii::t('app','РАЙОН')?>: </p>
                                                <div class="info_all_second-select-left-1">
                                                        <select class="rayon" name="rayon" required="required">
                                                            <option disabled selected><?= Yii::t('app','Выберите Район') ?></option>
                                                        </select>
                                                    <span>
	                                            	<i class="fa fa-sort-desc" aria-hidden="true"></i>
	                                            </span>
                                                </div>
                                            </div>
                                            <div class="info_all-leon">
                                                <p><?= Yii::t('app','ИНДЕКС') ?>: </p>
                                                <input type="text" name="index" value="<?= $user_info->index ?>">
                                            </div>
                                        </div>
                                        <div class="info_all_second-input-right">
                                            <div class="info_all-leon">
                                                <p>* <?= Yii::t('app','УЛИЦА') ?>:  </p>
                                                <input type="text" name="street" value="<?= $user_info->address ?>" required="required">
                                            </div>
                                            <div class="info_all-leon">
                                                <p>* <?= Yii::t('app','НОМЕР ДОМА') ?>:  </p>
                                                <input type="text" name="number_dom" value="<?= $user_info->number_dom ?>" required="required">
                                            </div>
                                            <div class="info_all-leon">
                                                <p><?= Yii::t('app','НОМЕР КВ') ?>.: </p>
                                                <input type="text" name="number_kv" value="<?= $user_info->number_kv ?>" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="info-tab_all-1-frs_btn">
                                        <button type="submit" class="info-_btn-red2"><?= Yii::t('app','сохранить') ?></button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>

    </div>
</section>

<section class="bascet_goods personal_ar_history">
    <div class="container">
        <div class="bascet_goods_all_h4">
            <div class="bascet_goods_all_h4_topp"><h4><?= Yii::t('app','ИСТОРИЯ ВАШИХ ЗАКАЗОВ') ?></h4></div>
            <div class="bascet_goods_6divs">
                <div class="bascet_goods_6divs-1">
                    <div class="bascet_goods_6-1">
                        <p>№</p>
                    </div>
                    <div class="bascet_goods_per_are">
                        <p><?= Yii::t('app','ДАТА') ?></p>
                    </div>
                    <div class="bascet_goods_6-2">
                        <p><?= Yii::t('app','ПРОДУКЦИЯ') ?></p>
                    </div>
                    <div class="bascet_goods_6-3">
                        <p><?= Yii::t('app','О ПРОДУКЦИИ') ?></p>
                    </div>
                    <div class="bascet_goods_6-4">
                        <p><?= Yii::t('app','ЦЕНА') ?></p>
                    </div>
                    <div class="bascet_goods_6-5">
                        <p><?= Yii::t('app','КОЛ-ВО') ?></p>
                    </div>
                    <div class="bascet_goods_6-6">
                        <p><?= Yii::t('app','ИТОГО') ?></p>
                    </div>
                </div>
                <?php if(!empty($orders)):  ?>
                <?php $k = 1; ?>
                    <?php foreach ($orders as $item): ?>
                        <?php foreach ($item->orderProducts as $product): ?>
                            <div class="bascet_goods_6divs-2" style="margin-bottom: 0px">
                                <div class="bascet_goods_62-1">
                                    <p><?= $k ?></p>
                                </div>
                                <div class="bascet_goods_per_are_bot">
                                    <p><?= date('d/m/Y',$item->created_at) ?></p>
                                </div>
                                <div class="bascet_goods_62-2">
                                    <div class="bascet_goods_62-2_img">
                                        <img src="/uploads/<?= $product->product->path ?>">
                                    </div>
                                </div>
                                <div class="bascet_goods_62-3">
                                    <div class="bascet_goods_62-3_all">
                                        <h6><?= $product->product->title ?></h6>
                                        <p>#<?= $product->product->code ?></p>
                                        <div class="bascet_goods_62-3_wo">
                                            <h4><?= Yii::t('app','РАЗМЕР') ?>:</h4>
                                            <h3><?= $product->size ?></h3>
                                        </div>
                                        <div class="bascet_goods_62-3_wo">
                                            <h4><?= Yii::t('app','ЦВЕТ') ?>:</h4>
                                            <h3><?= $product->color ?></h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="bascet_goods_62-4">
                                    <p><?= number_format($product->product_price,'0','.',' ')?> <?= Yii::t('app','сум') ?></p>
                                </div>
                                <div class="bascet_goods_62-5">
                                    <p><?= $product->quantity ?></p>
                                </div>
                                <div class="bascet_goods_62-6">
                                    <p><?= number_format($product->product_price*$product->quantity,'0','.',' ')?> <?= Yii::t('app','сум') ?></p>
                                </div>
                            </div>
                            <?php $k++;?>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                <?php else: ?>
                   <div class="bascet_goods_all_h4_topp"> <h4 style="margin-bottom: 100px" ><?= Yii::t('app','Вы еще ничего не заказали') ?></h4></div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

