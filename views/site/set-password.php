
<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 10.06.2018
 * Time: 11:03
 */

use yii\helpers\Url;

?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<section class="set_password_ek login_ek">
    <div class="container">
        <div class="set_password_ek_all login_ek_2div">
            <div class="login_ek_2div-1div">
                <h4><?= Yii::t('app','УСТАНОВИТЬ ПАРОЛЬ') ?></h4>
                <form action="<?= Url::to(['/site/set-password']) ?>" method="get" id="set-pass">
                    <input type="hidden" name="id" value="<?= $id ?>">
                    <div class="set_password_ek_diva">
                        <p>*<?= Yii::t('app','Пароль') ?> </p>
                        <input type="password" name="password" class="set-password">
                        <p>*<?= Yii::t('app','Подтвердите пароль') ?> </p>
                        <input type="password" name="password-com" class="set-password-com">
                        <button class="set-password-btn"><?= Yii::t('app','УСТАНОВИТЬ ПАРОЛЬ') ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
