<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php if(!empty($about)):?>
<!-- about top -->
<section class="about_free" style="background-image: url(/uploads/<?= $about->path ?>)">
    <div class="container">
        <div class="about_free_h1">
            <h1><?= $about->text1 ?></h1>
            <h2><?= $about->text2 ?></h2>
        </div>
    </div>
</section>
<!-- about us content -->

<section class="about_content">
    <div class="container">
        <div class="about_content_div">
            <div class="abo_content_h1">
                <h1><?= $about->title ?></h1>
            </div>
            <div class="about_content_text">
                <p><?= $about->content ?></p>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<!-- divs slider -->
<section class="divs4_slider">

    <div class="container">
        <div class="divs4_slider_btn">
            <button type="button" class="slick-prev4"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
            <button type="button" class="slick-next4"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
        </div>
        <div class="single-item_ek4">
            <!-- 	<div class="divs4_1"> -->
            <?php if(!empty($slider)): ?>
            <?php $count_slider = count($slider) ?>

            <?php if($count_slider == 1): ?>
                <?php foreach ($slider as $item): ?>
                    <div class="divs4_1_left">
                        <h3><?= Yii::t('app','Новинка') ?></h3>
                        <h2><?= $item->title ?></h2>
                        <div class="divs4_1_left_bi_img">
                            <img src="/uploads/<?= $item->path ?>">
                        </div>
                        <a href="<?= Url::to(['site/product','id' => $item->product_id]) ?>"><?= Yii::t('app','СМОТРЕТЬ') ?></a>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
            <?php if($count_slider == 2): ?>
                <?php foreach ($slider as $item): ?>
                    <div class="divs4_1_left">
                        <h3><?= Yii::t('app','Новинка') ?></h3>
                        <h2><?= $item->title ?></h2>
                        <div class="divs4_1_left_bi_img">
                            <img src="/uploads/<?= $item->path ?>">
                        </div>
                        <a href="<?= Url::to(['site/product','id' => $item->product_id]) ?>"><?= Yii::t('app','СМОТРЕТЬ') ?></a>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
            <?php if($count_slider == 3): ?>
                <?php foreach ($slider as $k => $item): ?>
                    <?php if($k == 0): ?>
                        <div class="divs4_1_left">
                            <h3><?= Yii::t('app','Новинка') ?></h3>
                            <h2><?= $item->title ?></h2>
                            <div class="divs4_1_left_bi_img">
                                <img src="/uploads/<?= $item->path ?>">
                            </div>
                            <a href="<?= Url::to(['site/product','id' => $item->product_id]) ?>"><?= Yii::t('app','СМОТРЕТЬ') ?></a>
                        </div>
                    <?php endif; ?>
                    <?php if($k == 1): ?>
                        <div class="divs4_1_right">
                        <div class="divs4_right-top">
                            <div class="divs4_right-top_tpim">
                                <img src="/uploads/<?= $item->path ?>">
                            </div>
                            <div class="divs4_right-top_right">
                                <h3><?= Yii::t('app','Внимание') ?></h3>
                                <h2><?= $item->title ?></h2>
                                <a href="<?= Url::to(['site/product','id' => $item->product_id]) ?>"><?= Yii::t('app','СМОТРЕТЬ') ?></a>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($k == 2): ?>
                        <div class="divs4_right-top">
                            <div class="divs4_right-top_tpim">
                                <img src="/uploads/<?= $item->path ?>">
                            </div>
                            <div class="divs4_right-top_right">
                                <h3><?= Yii::t('app','Внимание') ?></h3>
                                <h2><?= $item->title ?></h2>
                                <a href="<?= Url::to(['site/product','id' => $item->product_id]) ?>"><?= Yii::t('app','СМОТРЕТЬ') ?></a>
                            </div>
                        </div>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
            <?php if($count_slider == 4): ?>
            <?php foreach ($slider as $k => $item): ?>
            <?php if($k == 0): ?>
                <div class="divs4_1_left">
                    <h3><?= Yii::t('app','Новинка') ?></h3>
                    <h2><?= $item->title ?></h2>
                    <div class="divs4_1_left_bi_img">
                        <img src="/uploads/<?= $item->path ?>">
                    </div>
                    <a href="<?= Url::to(['site/product','id' => $item->product_id]) ?>"><?= Yii::t('app','СМОТРЕТЬ') ?></a>
                </div>
            <?php endif; ?>
            <?php if($k == 1): ?>
            <div class="divs4_1_right">
                <div class="divs4_right-top">
                    <div class="divs4_right-top_tpim">
                        <img src="/uploads/<?= $item->path ?>">
                    </div>
                    <div class="divs4_right-top_right">
                        <h3><?= Yii::t('app','Внимание') ?></h3>
                        <h2><?= $item->title ?></h2>
                        <a href="<?= Url::to(['site/product','id' => $item->product_id]) ?>"><?= Yii::t('app','СМОТРЕТЬ') ?></a>
                    </div>
                </div>

                <?php endif; ?>
                <?php if($k == 2): ?>
                <div class="divs4_right-botton">
                    <div class="divs4_right-botton1">
                        <a href="<?= Url::to(['site/product','id' => $item->product_id]) ?>">
                            <div class="divs4_right-botton1_imgdv">
                                <img src="/uploads/<?= $item->path ?>">
                            </div>
                            <h4><?= Yii::t('app','Лучшие для') ?></h4>
                            <h5><?= $item->title ?></h5>
                        </a>
                    </div>
                    <?php endif; ?>
                    <?php if($k == 3): ?>

                    <div class="divs4_right-botton2">
                        <a href="<?= Url::to(['site/product','id' => $item->product_id]) ?>" >
                            <h4><?= Yii::t('app','Это') ?></h4>
                            <h5 style="text-transform: uppercase"><?= $item->title ?></h5>
                            <div class="divs4_right-botton2_div_img">
                                <img src="/uploads/<?= $item->path ?>"
                            </div>
                        </a>
                    </div>

                </div>
            </div>
        </div>
        <?php endif; ?>
        <?php endforeach; ?>
        <?php endif; ?>

        <?php endif; ?>

        <?php if(!empty($slider2)): ?>
            <!-- <div  class="divs4_1"> -->
            <?php $count_slider2 = count($slider2) ?>

            <?php if($count_slider2 == 1): ?>
                <?php foreach ($slider2 as $item): ?>
                    <div class="divs4_1_left">
                        <h3><?= Yii::t('app','Новинка') ?></h3>
                        <h2><?= $item->title ?></h2>
                        <div class="divs4_1_left_bi_img">
                            <img src="/uploads/<?= $item->path ?>">
                        </div>
                        <a href="<?= Url::to(['site/product','id' => $item->product_id]) ?>"><?= Yii::t('app','СМОТРЕТЬ') ?></a>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
            <?php if($count_slider2 == 2): ?>
                <?php foreach ($slider2 as $item): ?>
                    <div class="divs4_1_left">
                        <h3><?= Yii::t('app','Новинка') ?></h3>
                        <h2><?= $item->title ?></h2>
                        <div class="divs4_1_left_bi_img">
                            <img src="/uploads/<?= $item->path ?>">
                        </div>
                        <a href="<?= Url::to(['site/product','id' => $item->product_id]) ?>"><?= Yii::t('app','СМОТРЕТЬ') ?></a>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
            <?php if($count_slider2 == 3): ?>
                <?php foreach ($slider2 as $k => $item): ?>
                    <?php if($k == 0): ?>
                        <div class="divs4_1_left">
                            <h3><?= Yii::t('app','Новинка') ?></h3>
                            <h2><?= $item->title ?></h2>
                            <div class="divs4_1_left_bi_img">
                                <img src="/uploads/<?= $item->path ?>">
                            </div>
                            <a href="<?= Url::to(['site/product','id' => $item->product_id]) ?>"><?= Yii::t('app','СМОТРЕТЬ') ?></a>
                        </div>
                    <?php endif; ?>
                    <?php if($k == 1): ?>
                        <div class="divs4_1_right">
                        <div class="divs4_right-top">
                            <div class="divs4_right-top_tpim">
                                <img src="/uploads/<?= $item->path ?>">
                            </div>
                            <div class="divs4_right-top_right">
                                <h3><?= Yii::t('app','Внимание') ?></h3>
                                <h2><?= $item->title ?></h2>
                                <a href="<?= Url::to(['site/product','id' => $item->product_id]) ?>"><?= Yii::t('app','СМОТРЕТЬ') ?></a>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($k == 2): ?>
                        <div class="divs4_right-top">
                            <div class="divs4_right-top_tpim">
                                <img src="/uploads/<?= $item->path ?>">
                            </div>
                            <div class="divs4_right-top_right">
                                <h3><?= Yii::t('app','Внимание') ?></h3>
                                <h2><?= $item->title ?></h2>
                                <a href="<?= Url::to(['site/product','id' => $item->product_id]) ?>"><?= Yii::t('app','СМОТРЕТЬ') ?></a>
                            </div>
                        </div>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
            <?php if($count_slider2 == 4): ?>
                <?php foreach ($slider2 as $k => $item): ?>
                    <?php if($k == 0): ?>
                        <div class="divs4_1_left">
                            <h3><?= Yii::t('app','Новинка') ?></h3>
                            <h2><?= $item->title ?></h2>
                            <div class="divs4_1_left_bi_img">
                                <img src="/uploads/<?= $item->path ?>">
                            </div>
                            <a href="<?= Url::to(['site/product','id' => $item->product_id]) ?>"><?= Yii::t('app','СМОТРЕТЬ') ?></a>
                        </div>
                    <?php endif; ?>
                    <?php if($k == 1): ?>
                        <div class="divs4_1_right">
                        <div class="divs4_right-top">
                            <div class="divs4_right-top_tpim">
                                <img src="/uploads/<?= $item->path ?>">
                            </div>
                            <div class="divs4_right-top_right">
                                <h3><?= Yii::t('app','Внимание') ?></h3>
                                <h2><?= $item->title ?></h2>
                                <a href="<?= Url::to(['site/product','id' => $item->product_id]) ?>"><?= Yii::t('app','СМОТРЕТЬ') ?></a>
                            </div>
                        </div>

                    <?php endif; ?>
                    <?php if($k == 2): ?>
                    <div class="divs4_right-botton">
                    <div class="divs4_right-botton1">
                        <a href="<?= Url::to(['site/product','id' => $item->product_id]) ?>">
                            <div class="divs4_right-botton1_imgdv">
                                <img src="/uploads/<?= $item->path ?>">
                            </div>
                            <h4><?= Yii::t('app','Лучшие для') ?></h4>
                            <h5><?= $item->title ?></h5>
                        </a>
                    </div>
                <?php endif; ?>
                    <?php if($k == 3): ?>

                        <div class="divs4_right-botton2">
                            <a href="<?= Url::to(['site/product','id' => $item->product_id]) ?>" >
                                <h4><?= Yii::t('app','Это') ?></h4>
                                <h5 style="text-transform: uppercase"><?= $item->title ?></h5>
                                <div class="divs4_right-botton2_div_img">
                                    <img src="/uploads/<?= $item->path ?>"
                                </div>
                            </a>
                        </div>

                        </div>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
            <!-- 	</div> -->
        <?php endif; ?>
    </div>
    </div>

</section>

<!-- slider for brend -->
<section class="brend">
    <div class="container">
        <div class="brend_alldiv">
            <?php if(!empty($partners)): ?>
                <?php foreach ($partners as $item): ?>
                    <div class="brend-imgdiv"><img src="/uploads/<?= $item->path ?>"></div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</section>
<!-- pay section -->
<section class="pay_4">
    <div class="container">
        <div class="pay_all">
            <?php if(!empty($payment)): ?>
                <div class="pay_all1div">
                <?php foreach ($payment as $k => $item): ?>
                    <div class="pay">
                        <img src="/uploads/<?= $item->path ?>">
                    </div>
                    <?php if($k == 1): ?>
                        </div>
                        <div class="pay_all1div">
                    <?php endif; ?>
                <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>
