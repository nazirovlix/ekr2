<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error">
    <section class="welcome thank_you">
        <div class="container">
            <div class="welcome_2divs">

                <div class="welcome_2divs_2">
                    <h2 style="font-size: 80px">404</h2>
                    <h2><?= Yii::t('app','Страница не найдена!') ?></h2>



                </div>
            </div>
        </div>
    </section>

</div>
