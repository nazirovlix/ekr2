<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>


<!-- login section -->
<section class="login_ek">
    <div class="container">
        <div class="login_ek_2div">

            <div class="login_ek_2div-1div">
                <h4><?= Yii::t('app', 'ВХОД') ?></h4>
                <?php $form = ActiveForm::begin([
                    'id' => 'login-form',
                    'action' => Url::to(['site/login'])
                ]); ?>

                <?= $form->field($model, 'username', ['template' => '{input}{error}', 'options' => ['tag' => false]])->textInput(['placeholder' => 'Логин'])->label(false) ?>

                <?= $form->field($model, 'password', ['template' => '{input}{error}', 'options' => ['tag' => false]])->passwordInput(['placeholder' => 'Пароль'])->label(false) ?>

                <?= Html::submitButton('ВХОД', ['style' => 'margin-top:0px']) ?>

                <?php ActiveForm::end(); ?>

                <a href="<?= Url::to(['site/restore-password']) ?>"><?= Yii::t('app', 'Забыли пароль') ?>?</a>
            </div>

            <div class="login_ek_2div-2div">
                <h4><?= Yii::t('app', 'ЗАРЕГИСТРИРОВАТЬСЯ') ?></h4>
                <p><?= Yii::t('app', 'Зарегистрируйся на <span>EKROSSOVKI.UZ</span> чтобы получить доступ к своим заказам, отслеживать
                    посылки или создавать список покупок.') ?></p>
                <a href="<?= Url::to(['site/sign-up']) ?>"><?= Yii::t('app', 'СОЗДАТЬ УЧЕТНУЮ ЗАПИСЬ') ?></a>
            </div>

        </div>
    </div>
</section>


