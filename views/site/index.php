<?php

/* @var $this yii\web\View */

use app\models\Favorites;
use yii\helpers\Url;

$this->title = 'Ekrossovki.uz';
?>
<!-- slider -->
<section class="slider_top">
    <div class="slick-ab-btnimgbac">
        <button type="button" class="slick-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
        <button type="button" class="slick-next"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
    </div>
    <div class="single-item_ek">
        <?php foreach ($carousel as $item): ?>
        <div class="single-item_ek_img" style="background-image: url(/uploads/<?=$item->path ?>);">
            <div class="single_content">
                <h5><?= Yii::t('app','Всего за') ?> <?= number_format($item->price,'0','.',' ') ?> <?= Yii::t('app','сум') ?></h5>
                <h1><?= $item->title ?></h1>
                <h4><?= Yii::t('app','Для мужчин') ?></h4>
                <p><?= $item->content ?></p>
                <a href="<?= Url::to(['/product/product','id' => $item->product_id]) ?>"><?= Yii::t('app','СМОТРЕТЬ') ?></a>
            </div>
        </div>
        <?php endforeach; ?>

    </div>
</section>
<!-- divs slider -->
<section class="divs4_slider">

    <div class="container">
        <div class="divs4_slider_btn">
            <button type="button" class="slick-prev4"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
            <button type="button" class="slick-next4"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
        </div>
        <div class="single-item_ek4">
            <!-- 	<div class="divs4_1"> -->
            <?php if(!empty($slider)): ?>
            <?php $count_slider = count($slider) ?>

                <?php if($count_slider == 1): ?>
                    <?php foreach ($slider as $item): ?>
                        <div class="divs4_1_left">
                            <h3><?= Yii::t('app','Новинка') ?></h3>
                            <h2><?= $item->title ?></h2>
                            <div class="divs4_1_left_bi_img">
                                <img src="/uploads/<?= $item->path ?>">
                            </div>
                            <a href="<?= Url::to(['site/product','id' => $item->product_id]) ?>"><?= Yii::t('app','СМОТРЕТЬ') ?></a>
                        </div>
                    <?php endforeach; ?>
               <?php endif; ?>
                <?php if($count_slider == 2): ?>
                    <?php foreach ($slider as $item): ?>
                        <div class="divs4_1_left">
                            <h3><?= Yii::t('app','Новинка') ?></h3>
                            <h2><?= $item->title ?></h2>
                            <div class="divs4_1_left_bi_img">
                                <img src="/uploads/<?= $item->path ?>">
                            </div>
                            <a href="<?= Url::to(['site/product','id' => $item->product_id]) ?>"><?= Yii::t('app','СМОТРЕТЬ') ?></a>
                        </div>
                    <?php endforeach; ?>
               <?php endif; ?>
                <?php if($count_slider == 3): ?>
                    <?php foreach ($slider as $k => $item): ?>
                        <?php if($k == 0): ?>
                            <div class="divs4_1_left">
                                <h3><?= Yii::t('app','Новинка') ?></h3>
                                <h2><?= $item->title ?></h2>
                                <div class="divs4_1_left_bi_img">
                                    <img src="/uploads/<?= $item->path ?>">
                                </div>
                                <a href="<?= Url::to(['site/product','id' => $item->product_id]) ?>"><?= Yii::t('app','СМОТРЕТЬ') ?></a>
                            </div>
                        <?php endif; ?>
                        <?php if($k == 1): ?>
                        <div class="divs4_1_right">
                            <div class="divs4_right-top">
                                <div class="divs4_right-top_tpim">
                                    <img src="/uploads/<?= $item->path ?>">
                                </div>
                                <div class="divs4_right-top_right">
                                    <h3><?= Yii::t('app','Внимание') ?></h3>
                                    <h2><?= $item->title ?></h2>
                                    <a href="<?= Url::to(['site/product','id' => $item->product_id]) ?>"><?= Yii::t('app','СМОТРЕТЬ') ?></a>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if($k == 2): ?>
                            <div class="divs4_right-top">
                                <div class="divs4_right-top_tpim">
                                    <img src="/uploads/<?= $item->path ?>">
                                </div>
                                <div class="divs4_right-top_right">
                                    <h3><?= Yii::t('app','Внимание') ?></h3>
                                    <h2><?= $item->title ?></h2>
                                    <a href="<?= Url::to(['site/product','id' => $item->product_id]) ?>"><?= Yii::t('app','СМОТРЕТЬ') ?></a>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
               <?php endif; ?>
                <?php if($count_slider == 4): ?>
                    <?php foreach ($slider as $k => $item): ?>
                        <?php if($k == 0): ?>
                            <div class="divs4_1_left">
                                <h3><?= Yii::t('app','Новинка') ?></h3>
                                <h2><?= $item->title ?></h2>
                                <div class="divs4_1_left_bi_img">
                                    <img src="/uploads/<?= $item->path ?>">
                                </div>
                                <a href="<?= Url::to(['site/product','id' => $item->product_id]) ?>"><?= Yii::t('app','СМОТРЕТЬ') ?></a>
                            </div>
                        <?php endif; ?>
                        <?php if($k == 1): ?>
                            <div class="divs4_1_right">
                                <div class="divs4_right-top">
                                    <div class="divs4_right-top_tpim">
                                        <img src="/uploads/<?= $item->path ?>">
                                    </div>
                                    <div class="divs4_right-top_right">
                                        <h3><?= Yii::t('app','Внимание') ?></h3>
                                        <h2><?= $item->title ?></h2>
                                        <a href="<?= Url::to(['site/product','id' => $item->product_id]) ?>"><?= Yii::t('app','СМОТРЕТЬ') ?></a>
                                    </div>
                                </div>

                        <?php endif; ?>
                        <?php if($k == 2): ?>
                            <div class="divs4_right-botton">
                                <div class="divs4_right-botton1">
                                    <a href="<?= Url::to(['site/product','id' => $item->product_id]) ?>">
                                        <div class="divs4_right-botton1_imgdv">
                                            <img src="/uploads/<?= $item->path ?>">
                                        </div>
                                        <h4><?= Yii::t('app','Лучшие для') ?></h4>
                                        <h5><?= $item->title ?></h5>
                                    </a>
                                </div>
                        <?php endif; ?>
                        <?php if($k == 3): ?>

                            <div class="divs4_right-botton2">
                                <a href="<?= Url::to(['site/product','id' => $item->product_id]) ?>" >
                                    <h4><?= Yii::t('app','Это') ?></h4>
                                    <h5 style="text-transform: uppercase"><?= $item->title ?></h5>
                                    <div class="divs4_right-botton2_div_img">
                                        <img src="/uploads/<?= $item->path ?>"
                                    </div>
                                </a>
                            </div>

                        </div>
                        </div>
                        </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
               <?php endif; ?>

            <?php endif; ?>

            <?php if(!empty($slider2)): ?>
            <!-- <div  class="divs4_1"> -->
                <?php $count_slider2 = count($slider2) ?>

                <?php if($count_slider2 == 1): ?>
                    <?php foreach ($slider2 as $item): ?>
                        <div class="divs4_1_left">
                            <h3><?= Yii::t('app','Новинка') ?></h3>
                            <h2><?= $item->title ?></h2>
                            <div class="divs4_1_left_bi_img">
                                <img src="/uploads/<?= $item->path ?>">
                            </div>
                            <a href="<?= Url::to(['site/product','id' => $item->product_id]) ?>"><?= Yii::t('app','СМОТРЕТЬ') ?></a>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
                <?php if($count_slider2 == 2): ?>
                    <?php foreach ($slider2 as $item): ?>
                        <div class="divs4_1_left">
                            <h3><?= Yii::t('app','Новинка') ?></h3>
                            <h2><?= $item->title ?></h2>
                            <div class="divs4_1_left_bi_img">
                                <img src="/uploads/<?= $item->path ?>">
                            </div>
                            <a href="<?= Url::to(['site/product','id' => $item->product_id]) ?>"><?= Yii::t('app','СМОТРЕТЬ') ?></a>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
                <?php if($count_slider2 == 3): ?>
                    <?php foreach ($slider2 as $k => $item): ?>
                        <?php if($k == 0): ?>
                            <div class="divs4_1_left">
                                <h3><?= Yii::t('app','Новинка') ?></h3>
                                <h2><?= $item->title ?></h2>
                                <div class="divs4_1_left_bi_img">
                                    <img src="/uploads/<?= $item->path ?>">
                                </div>
                                <a href="<?= Url::to(['site/product','id' => $item->product_id]) ?>"><?= Yii::t('app','СМОТРЕТЬ') ?></a>
                            </div>
                        <?php endif; ?>
                        <?php if($k == 1): ?>
                            <div class="divs4_1_right">
                            <div class="divs4_right-top">
                                <div class="divs4_right-top_tpim">
                                    <img src="/uploads/<?= $item->path ?>">
                                </div>
                                <div class="divs4_right-top_right">
                                    <h3><?= Yii::t('app','Внимание') ?></h3>
                                    <h2><?= $item->title ?></h2>
                                    <a href="<?= Url::to(['site/product','id' => $item->product_id]) ?>"><?= Yii::t('app','СМОТРЕТЬ') ?></a>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if($k == 2): ?>
                            <div class="divs4_right-top">
                                <div class="divs4_right-top_tpim">
                                    <img src="/uploads/<?= $item->path ?>">
                                </div>
                                <div class="divs4_right-top_right">
                                    <h3><?= Yii::t('app','Внимание') ?></h3>
                                    <h2><?= $item->title ?></h2>
                                    <a href="<?= Url::to(['site/product','id' => $item->product_id]) ?>"><?= Yii::t('app','СМОТРЕТЬ') ?></a>
                                </div>
                            </div>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
                <?php if($count_slider2 == 4): ?>
                    <?php foreach ($slider2 as $k => $item): ?>
                        <?php if($k == 0): ?>
                            <div class="divs4_1_left">
                                <h3><?= Yii::t('app','Новинка') ?></h3>
                                <h2><?= $item->title ?></h2>
                                <div class="divs4_1_left_bi_img">
                                    <img src="/uploads/<?= $item->path ?>">
                                </div>
                                <a href="<?= Url::to(['site/product','id' => $item->product_id]) ?>"><?= Yii::t('app','СМОТРЕТЬ') ?></a>
                            </div>
                        <?php endif; ?>
                        <?php if($k == 1): ?>
                            <div class="divs4_1_right">
                            <div class="divs4_right-top">
                                <div class="divs4_right-top_tpim">
                                    <img src="/uploads/<?= $item->path ?>">
                                </div>
                                <div class="divs4_right-top_right">
                                    <h3><?= Yii::t('app','Внимание') ?></h3>
                                    <h2><?= $item->title ?></h2>
                                    <a href="<?= Url::to(['site/product','id' => $item->product_id]) ?>"><?= Yii::t('app','СМОТРЕТЬ') ?></a>
                                </div>
                            </div>

                        <?php endif; ?>
                        <?php if($k == 2): ?>
                        <div class="divs4_right-botton">
                        <div class="divs4_right-botton1">
                            <a href="<?= Url::to(['site/product','id' => $item->product_id]) ?>">
                                <div class="divs4_right-botton1_imgdv">
                                    <img src="/uploads/<?= $item->path ?>">
                                </div>
                                <h4><?= Yii::t('app','Лучшие для') ?></h4>
                                <h5><?= $item->title ?></h5>
                            </a>
                        </div>
                    <?php endif; ?>
                        <?php if($k == 3): ?>

                            <div class="divs4_right-botton2">
                                <a href="<?= Url::to(['site/product','id' => $item->product_id]) ?>" >
                                    <h4><?= Yii::t('app','Это') ?></h4>
                                    <h5 style="text-transform: uppercase"><?= $item->title ?></h5>
                                    <div class="divs4_right-botton2_div_img">
                                        <img src="/uploads/<?= $item->path ?>"
                                    </div>
                                </a>
                            </div>

                            </div>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            <!-- 	</div> -->
            <?php endif; ?>
        </div>
    </div>
</section>
<!-- for woman -->
<section class="for_woman">
    <div class="container">
        <h1><?= Yii::t('app','ДЛЯ ЖЕНЩИН') ?></h1>

        <?php if(!empty($forwoman)): ?>
            <div class="for_woman_8">
                <?php foreach($forwoman as $item): ?>
                    <div class="for_woman-1div">

                        <?php if(!empty($item->discount)):?>
                            <h5>-<?= $item->discount ?>%</h5>
                            <h3><?= number_format($item->discount_price,'0','.',' ') ?> <?= Yii::t('app','сум') ?></h3>
                            <h4><?= number_format($item->price,'0','.',' ') ?> <?= Yii::t('app','сум') ?></h4>
                        <?php else: ?>
                            <h3><?= number_format($item->price,'0','.',' ') ?> <?= Yii::t('app','сум') ?></h3>
                        <?php endif ?>
                        <div class="for_woman-1div_imgd">
                            <a href="<?= Url::to(['/product/product','id' => $item->id]) ?>"><img src="/uploads/<?= $item->path ?>"></a>
                        </div>
                        <a href="<?= Url::to(['/product/product','id' => $item->id]) ?>"><h6><?= $item->title ?></h6></a>
                        <div class="for_woman-1div_fev">
                            <p>#<?= $item->code ?></p>
                            <div class="woman-1div_fev_card">
                                <?php
                                if(Yii::$app->user->isGuest):

                                    $ff = 0;
                                    if(!empty(Yii::$app->session['favorites'])){
                                        $f = $_SESSION['favorites'];
                                        foreach ($f as $fav):
                                            if($fav == $item->id){
                                                $ff = 1;
                                            }
                                        endforeach;
                                    }
                                    if($ff == 1):  ?>
                                        <div class="woman_like liked_items_main del-favorite" data-product="<?= $item->id ?>"></div>
                                    <?php else: ?>
                                        <div class="woman_like add-favorite" data-product="<?= $item->id ?>"></div>
                                    <?php  endif;
                                else:
                                    $favorites = Favorites::find()->where(['product_id'=> $item->id, 'user_id' => Yii::$app->user->id])->one();;
                                    if(!empty($favorites)){ ?>
                                        <div class="woman_like liked_items_main del-favorite" data-product="<?= $item->id ?>"></div>
                                    <?php } else{ ?>
                                        <div class="woman_like add-favorite" data-product="<?= $item->id ?>"></div>
                                    <?php    }
                                endif;
                                ?>
                            </div>
                        </div>

                    </div>
                <?php endforeach; ?>

            </div>
            <div class="woman_link">
                <a href="<?= Url::to(['/site/category','id'=>1]) ?>"><?= Yii::t('app','ПОКАЗАТЬ ЕЩЁ') ?></a>
            </div>
        <?php else: ?>
            <h3 style="margin: 10px"><?= Yii::t('app','Продукты еще не добавлены') ?></h3>
        <?php endif; ?>
    </div>
</section>
<!-- for men -->
<section class="for_woman">
    <div class="container">
        <h1><?= Yii::t('app','ДЛЯ МУЖЧИН')  ?></h1>
        <?php if(!empty($forman)): ?>
            <div class="for_woman_8">
                <?php foreach($forman as $item): ?>
                    <div class="for_woman-1div">

                        <?php if(!empty($item->discount)):?>
                            <h5>-<?= $item->discount ?>%</h5>
                            <h3><?= number_format($item->discount_price,'0','.',' ') ?> <?= Yii::t('app','сум') ?></h3>
                            <h4><?= number_format($item->price,'0','.',' ') ?> <?= Yii::t('app','сум') ?></h4>
                        <?php else: ?>
                            <h3><?= number_format($item->price,'0','.',' ') ?> <?= Yii::t('app','сум') ?></h3>
                        <?php endif ?>
                        <div class="for_woman-1div_imgd">
                            <a href="<?= Url::to(['/product/product','id' => $item->id]) ?>"><img src="/uploads/<?= $item->path ?>"></a>
                        </div>
                        <a href="<?= Url::to(['/product/product','id' => $item->id]) ?>"><h6><?= $item->title ?></h6></a>
                        <div class="for_woman-1div_fev">
                            <p>#<?= $item->code ?></p>
                            <div class="woman-1div_fev_card">
                                <?php
                                if(Yii::$app->user->isGuest):
                                    $ff = 0;
                                    if(!empty(Yii::$app->session['favorites'])){
                                        $f = $_SESSION['favorites'];
                                        foreach ($f as $fav):
                                            if($fav == $item->id){
                                                $ff = 1;
                                            }
                                        endforeach;
                                    }

                                    if($ff == 1):  ?>
                                        <div class="woman_like liked_items_main del-favorite" data-product="<?= $item->id ?>"></div>
                                    <?php else: ?>
                                        <div class="woman_like add-favorite" data-product="<?= $item->id ?>"></div>
                                    <?php  endif;
                                else:
                                    $favorites = Favorites::find()->where(['product_id'=> $item->id, 'user_id' => Yii::$app->user->id])->one();
                                    if(!empty($favorites)){ ?>
                                      <div class="woman_like liked_items_main del-favorite" data-product="<?= $item->id ?>"></div>
                                    <?php } else{ ?>
                                      <div class="woman_like add-favorite" data-product="<?= $item->id ?>"></div>
                                    <?php    }
                                endif;

                                ?>


                            </div>
                        </div>

                    </div>
                <?php endforeach; ?>

        </div>
        <div class="woman_link">
            <a href="<?= Url::to(['/site/category','id'=>1]) ?>"><?= Yii::t('app','ПОКАЗАТЬ ЕЩЁ') ?></a>
        </div>
        <?php else: ?>
            <h3 style="margin: 10px"><?= Yii::t('app','Продукты еще не добавлены') ?></h3>
        <?php endif; ?>
    </div>
</section>
<!-- for children -->
<section class="for_woman">
    <div class="container">
        <h1><?= Yii::t('app','ДЛЯ ДЕТЕЙ') ?></h1>

        <?php if(!empty($forchild)): ?>
            <div class="for_woman_8">
                <?php foreach($forchild as $item): ?>
                    <div class="for_woman-1div">

                        <?php if(!empty($item->discount)):?>
                            <h5>-<?= $item->discount ?>%</h5>
                            <h3><?= number_format($item->discount_price,'0','.',' ') ?> <?= Yii::t('app','сум') ?></h3>
                            <h4><?= number_format($item->price,'0','.',' ') ?> <?= Yii::t('app','сум') ?></h4>
                        <?php else: ?>
                            <h3><?= number_format($item->price,'0','.',' ') ?> <?= Yii::t('app','сум') ?></h3>
                        <?php endif ?>
                        <div class="for_woman-1div_imgd">
                            <a href="<?= Url::to(['/product/product','id' => $item->id]) ?>"><img src="/uploads/<?= $item->path ?>"></a>
                        </div>
                        <a href="<?= Url::to(['/product/product','id' => $item->id]) ?>"><h6><?= $item->title ?></h6></a>
                        <div class="for_woman-1div_fev">
                            <p>#<?= $item->code ?></p>
                            <div class="woman-1div_fev_card">
                                <?php
                                if(Yii::$app->user->isGuest):

                                    $ff = 0;
                                    if(!empty(Yii::$app->session['favorites'])){
                                        $f = $_SESSION['favorites'];
                                        foreach ($f as $fav):
                                            if($fav == $item->id){
                                                $ff = 1;
                                            }
                                        endforeach;
                                    }
                                    if($ff == 1):  ?>
                                        <div class="woman_like liked_items_main del-favorite" data-product="<?= $item->id ?>"></div>
                                    <?php else: ?>
                                        <div class="woman_like add-favorite" data-product="<?= $item->id ?>"></div>
                                    <?php  endif;
                                else:
                                    $favorites = Favorites::find()->where(['product_id'=> $item->id, 'user_id' => Yii::$app->user->id])->one();
                                    if(!empty($favorites)){ ?>
                                        <div class="woman_like liked_items_main del-favorite" data-product="<?= $item->id ?>"></div>
                                    <?php } else{ ?>
                                        <div class="woman_like add-favorite" data-product="<?= $item->id ?>"></div>
                                    <?php    }
                                endif;

                                ?>


                            </div>
                        </div>

                    </div>
                <?php endforeach; ?>

            </div>
            <div class="woman_link">
                <a href="<?= Url::to(['/site/category','id'=>1]) ?>"><?= Yii::t('app','ПОКАЗАТЬ ЕЩЁ') ?></a>
            </div>
        <?php else: ?>
            <h3 style="margin: 10px"><?= Yii::t('app','Продукты еще не добавлены') ?></h3>
        <?php endif; ?>
    </div>
</section>
<!-- slider for brend -->
<section class="brend">
    <div class="container">
        <div class="brend_alldiv">
            <?php if(!empty($partners)): ?>
            <?php foreach ($partners as $item): ?>
                <div class="brend-imgdiv"><img src="/uploads/<?= $item->path ?>"></div>
            <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</section>
<!-- pay section -->
<section class="pay_4">
    <div class="container">
        <div class="pay_all">
            <?php if(!empty($payment)): ?>
            <div class="pay_all1div">
                <?php foreach ($payment as $k => $item): ?>
                    <div class="pay">
                        <img src="/uploads/<?= $item->path ?>">
                    </div>
                    <?php if($k == 1): ?>
                        </div>
                        <div class="pay_all1div">
                    <?php endif; ?>
               <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>
