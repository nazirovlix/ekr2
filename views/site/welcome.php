<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 10.06.2018
 * Time: 13:06
 */

use yii\helpers\Url;

?>

    <section class="welcome">
        <div class="container">
            <div class="welcome_2divs">
                <div class="welcome_2divs_1">
                    <h1><?= Yii::t('app','Добро Пожаловать!')?></h1>
                </div>
                <div class="welcome_2divs_2">
                    <h2><?= Yii::t('app','Вы создали аккаунт на сайте') ?> <span>EKROSSOVKI.UZ</span></h2>
                    <p><?= Yii::t('app','Добро пожаловать!') ?></p>
                    <a href="<?= Url::to(['/site/login']) ?>"><?= Yii::t('app','Войти') ?></a>
                </div>
            </div>
        </div>
    </section>

