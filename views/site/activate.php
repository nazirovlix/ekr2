<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Activate user';
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="password-reset">
<!-- registration ekrossovki -->


            <div class="registration_top_h1 " >
                <h1><?=Yii::t('app','Регистрация аккаунта на сайте <span>EKROSSOVKI.UZ</span>')?></h1>
            </div>
            <div class="registration_making">
                <h4><?=Yii::t('app','Активировать аккаунт')?></h4>
                <h6>* <?=Yii::t('app','указывает обязательное поле')?></h6>
                <div class="registration_making_all">
                    <?php ActiveForm::begin(['id' => 'get-code', 'action' => Url::to(['site/activate']), 'method' => 'post']); ?>
                        <input type="hidden" name="id" value="<?= $user_info->user_id ?>">
                        <input type="hidden" name="phone" value="<?= $user_info->phone ?>">
                    <div class="registration_making-nball">

                        <div class="registration_making_all_2">
                            <h5>*<?=Yii::t('app','ТЕЛЕФОН НОМЕР')?>.: </h5>
                            <?= Html::input('text', 'phone2',$user_info->phone,['id' => 'phone', 'disabled' => true]) ?>
                        </div>
                        <div class="registration_making_all_3">
                            <button class="1get-code" data-url="<?= Url::to(['site/send-code']) ?>" ><?= Yii::t('app', 'ПОЛУЧИТЬ КОД') ?></button>
                            <h5>*<?=Yii::t('app','ВВЕДИТЕ КОД')?>: </h5>
                            <?= Html::input('text', 'code', '',['id' => 'code','required' => true]) ?>
                        </div>
                        <div class="registration_making_all_1_p">
                            <p><a href="javascript:void(0)" class="send-code-again" data-url="<?= Url::to(['site/send-again']) ?>" ><?= Yii::t('app', 'Попробуйте снова') ?></a></p>
                        </div>

                        <div class="registration_making_all-btn">
                            <button class="check-code" disabled><?=Yii::t('app','АКТИВИРОВАТЬ')?></button>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>

</div>