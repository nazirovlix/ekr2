<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Sign up';
$this->params['breadcrumbs'][] = $this->title;
?>


<!-- registration ekrossovki -->
<section class="registration_ek">
    <div class="container">
        <div class="registration_ek-all">
            <div class="registration_top_h1">
                <h1><?=Yii::t('app','Регистрация аккаунта на сайте')?><span>EKROSSOVKI.UZ</span></h1>
            </div>
            <div class="registration_making">
                <h4><?=Yii::t('app','Создать свой аккаунт')?></h4>
                <h6>* <?=Yii::t('app','указывает обязательное поле')?></h6>
                <div class="registration_making_all">
                    <?php ActiveForm::begin(['id' => 'sing-up', 'action' => Url::to(['site/sign-up']), 'method' => 'post']); ?>
                        <div class="registration_making-nball">
                            <div class="registration_making_all_1">
                                <h5>* <?=Yii::t('app','ЛОГИН')?>:</h5>
                                <?= Html::input('text', 'username', '',['required' => true]) ?>
                            </div>
                            <div class="registration_making_all_1_p">
                                <p><?=Yii::t('app','Это имя появляется, когда мы приветствуем вас на нашем сайте')?>.</p>
                            </div>
                            <div class="registration_making_all_2">
                                <h5>*<?=Yii::t('app','ТЕЛЕФОН НОМЕР')?>.: </h5>
                                <?= Html::input('tel', 'phone','',['id' => 'phone', 'required' => true,'pattern' => '[+]{1}[9]{2}[8]{1}[0,1,3,4,5,7,8,9]{1}[1-9]{1}[0-9]{7}','placeholder' => '+998901234567','maxlength'=> '13','minlength' => '9']) ?>
                            </div>
                            <div class="registration_making_all_4">
                                <h5><?=Yii::t('app','EMAIL')?>:</h5>
                                <?= Html::input('email', 'email', '',['required' => false]) ?>
                            </div>
                            <div class="registration_making_all_4">
                                <h5>*<?=Yii::t('app','ПАРОЛЬ')?>:</h5>
                                <?= Html::input('password', 'password', '',['required' => true, 'minlength' => '6','id' => 'password']) ?>
                            </div>
                            <div class="registration_making_all_1_p">
                                <p><?=Yii::t('app','Пароль должен содержать как минимум 6 символов')?></p>
                            </div>
                            <div class="registration_making_all_5">
                                <h5>*<?=Yii::t('app','ПОДТВЕРДИТЕ ПАРОЛЬ')?>: </h5>
                                <?= Html::input('password', 'confirmPassword', '',['required' => true,'minlength' => '6','id' => 'confirm_password']) ?>
                            </div>
                            <div class="registration_making_all-btn">
                                <button type="submit"><?=Yii::t('app','ЗАРЕГИСТРИРОВАТЬСЯ')?></button>
                            </div>
                        </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>