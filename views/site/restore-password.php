<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Restore password';
$this->params['breadcrumbs'][] = $this->title;
?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<style>
    .check-code[disabled]{
        cursor: not-allowed;
    }
</style>
<div id="password-reset">
<!-- restore password -->
<section class="restore_password registration_ek">
    <div class="container">
        <div class="registration_ek-all">
            <div class="registration_making">
                <h4><?= Yii::t('app', 'востановить пароль') ?></h4>
                <h6>* <?= Yii::t('app', 'указывает обязательное поле') ?></h6>
                <div class="registration_making_all">
                    <?php ActiveForm::begin(['id' => 'get-code', 'action' => Url::to(['site/send-code']), 'method' => 'post']); ?>
                    <div class="registration_making-nball">
                        <div class="restore_password_div1 registration_making_all_1">
                            <h5>* <?= Yii::t('app', 'НОМЕР ТЕЛЕФОНА') ?>:</h5>
                            <?= Html::input('text', 'phone', '', ['id' => 'phone', 'required' => true]) ?>
                        </div>

                        <div class="registration_making_all_1_p">
                            <p><?= Yii::t('app', 'Введите номер указаный при регистрации') ?></p>
                        </div>

                        <div class="restore_password_div2 registration_making_all_3">
                            <button class="1get-code"><?= Yii::t('app', 'ПОЛУЧИТЬ КОД') ?></button>
                            <h5>*<?= Yii::t('app', 'ВВЕДИТЕ КОД') ?>: </h5>
                            <?= Html::input('text', 'code', '', ['id' => 'code']) ?>
                        </div>

                        <div class="registration_making_all_1_p restore_passd">
                            <p><a href="javascript:void(0)" class="send-code-again" data-url="<?= Url::to(['site/send-again']) ?>" ><?= Yii::t('app', 'Попробуйте снова') ?></a></p>
                        </div>

                        <div class="registration_making_all-btn restore_passbtn">
                            <button class="check-code" ><?= Yii::t('app', 'ОТПРАВИТЬ') ?></button>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
