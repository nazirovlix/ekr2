<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\models\Application;
use app\models\Favorites;
use app\models\Info;
use app\models\Pages;
use app\models\Services;
use app\models\Socials;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

$info = Info::find()->where(['id' => 1])->one();
$social = Socials::find()->where(['status' => 1])->all();
$services = Services::find()->where(['status' => 1])->all();
$pages = Pages::find()->all();

$favs = Yii::$app->session['favorites'];
$model = new Application();
AppAsset::register($this);
?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<section>
    <!-- all headers -->
    <header>
        <div class="container">
            <div class="headerall-3">
                <div class="header1-div">
                    <p><?= Yii::t('app','Доступен 24/7 в') ?> <?= $info->phone  ?></p>
                </div>
                <div class="header2-div">
                    <ul>
                        <?php foreach ($social as $item): ?>
                        <li><a href="<?= $item->url  ?>"><img src="/uploads/<?= $item->path ?>"></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="header3-div">
                    <div class="languages">
                        <img src="/img/icon-language.png">
                        <select id="language"  data-url="<?= Url::to(['site/setlanguage']) ?>">
                        <?php if(Yii::$app->language == 'ru'): ?>
                                <option value="volvo">Русский</option>
                                <option value="saab">O'zbekcha</option>
                        <?php else: ?>
                                <option value="saab">O'zbekcha</option>
                                <option value="volvo">Русский</option>
                        <?php endif; ?>
                        </select>


                    </div>

                    <?php if(Yii::$app->user->isGuest): ?>
                    <div class="header3-div-register">
                        <img src="/img/Shape.png">
                        <button onClick="window.location.href='<?= Url::to(['/site/login']) ?>'">
                            <?= Yii::t('app','Войти')  ?>   <span>|</span> <?= Yii::t('app','Регистрация')  ?>
                        </button>
                    </div>
                    <?php else:  ?>
                        <div class="header3-div-register mypersonal">
                            <img src="/img/Shape.png">
                            <button>
                                <?= Yii::t('app','Личный кабинет') ?>
                            </button>
                            <ul>
                                <li><a href="<?= Url::to(['/site/cabinet']) ?>"><?= Yii::t('app','Мой кабинет') ?></a></li>
                                <li><a href="<?= Url::to(['/site/logout'])  ?>" data-method="POST"><?= Yii::t('app','Выйти') ?></a></li>
                            </ul>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </header>
    <!-- header search -->
    <div class="search-header">
        <div class="container">
            <div class="search_all-3">
                <div class="logo-header">
                    <a href="/">
                        <img src="/img/logotop.png">
                    </a>
                </div>
                <div class="search-2-div">
                    <form action="<?= Url::to(['/product/search']) ?>" method="get">
                        <input type="search" name="q" placeholder="<?= Yii::t('app','Поиск по сайту') ?>" required>
                        <button type="submit"><img src="/img/searchicon.png"></button>
                    </form>
                </div>
                <div class="search-3-div">
                    <div class="search-3-div-heard">
                        <a href="<?= Url::to(['product/favorites']) ?>">
                            <i class="fa fa-heart" aria-hidden="true"></i>
                            <?php if(Yii::$app->user->isGuest): ?>
                                <?php if(!empty($favs)): ?>
                                <span class="favorites-count"><?= count($favs) ?></span>
                                <?php endif; ?>
                            <?php else: ?>
                                <?php $fav = Favorites::find()->where(['user_id' => Yii::$app->user->id])->count();?>
                                <span class="favorites-count"><?= $fav ?></span>
                            <?php endif; ?>
                        </a>
                    </div>
                    <a class="card" href="<?= Url::to(['product/basket']) ?>">
                        <img src="/img/cardtop.png">
                        <div class="card-p">
                            <?php if(!empty(Yii::$app->session['cart'])): ?>
                            <?php $count = $_SESSION['cart.qty'];
                                    $sum = $_SESSION['cart.sum'] ?>
                            <?php else: $count =0; $sum = 0; ?>
                            <?php endif; ?>
                            <p>Корзина: <span><?= $count ?></span></p>
                            <p><?= number_format($sum, '0','.',' ') ?> сум</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="lnheader_navbar">
        <div class="container">
            <nav class="navbar navbar-inverse">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                   <?php $id = Yii::$app->request->get('id') ?>

                    <ul class="nav navbar-nav">
                        <li class="<?= Yii::$app->controller->action->id == 'index' ? 'active' : null ?>"><a href="/"><?= Yii::t('app','Главная') ?></a></li>
                        <li class="<?= Yii::$app->controller->action->id == 'about' ? 'active' : null ?>"><a href="<?= Url::to(['/site/about']) ?>"><?= Yii::t('app','О нас') ?></a></li>
                        <li class="<?= ((Yii::$app->controller->action->id == 'category')&&($id == 2)) ? 'active' : null ?>"><a href="<?= Url::to(['/product/category','id'=> 2]) ?>"><?= Yii::t('app','Для Женщин') ?></a></li>
                        <li class="<?= ((Yii::$app->controller->action->id == 'category')&&($id == 1)) ? 'active' : null ?>"><a href="<?= Url::to(['/product/category', 'id'=> 1]) ?>"><?= Yii::t('app','Для Мужчин') ?></a></li>
                        <li class="<?= ((Yii::$app->controller->action->id == 'category')&&($id == 3)) ? 'active' : null ?>"><a href="<?= Url::to(['/product/category', 'id'=> 3]) ?>"><?= Yii::t('app','Для Детей') ?></a></li>
                        <li class="drop_ol-my"><a href="#"><?= Yii::t('app','Информация для покупателя') ?> <span> <i class="fa fa-angle-down" aria-hidden="true"></i></span></a>
                            <ol class="drop_ol-my_block">
                                <?php if(!empty($pages)):  ?>
                                    <?php foreach ($pages as $k =>$item): ?>
                                        <?php if($k != 0):?>
                                        <li><a href="<?= Url::to(['/site/pages', 'id' => $item->id]) ?>"><?= $item->title ?></a></li>
                                        <?php endif;  ?>
                                    <?php endforeach; ?>
                                <?php endif;  ?>

                            </ol>
                        </li>
                    </ul>

                </div>

            </nav>
        </div>
    </div>
</section>

        <?= $content ?>


<!-- footer -->
<footer class="botton">


    <input class="hidden" value="<?= Yii::t('app', 'Спасибо!') ?>" id="swal1">
    <input class="hidden" value="<?= Yii::t('app', 'Ваша заявка принята') ?>" id="swal2">
    <?php if (Yii::$app->session->hasFlash('app')): ?>
        <?php $this->registerJs('
                                var swal_a = $("#swal1").val();
                                var swal_b = $("#swal2").val();
                                swal(swal_a, swal_b, "success");
                                '); ?>
    <?php endif; ?>
    <!-- modal for оформление заявки -->
    <div class="my_appl">
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><?= Yii::t('app','оформление заявки') ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="my_appl_mdbody">
                            <?php $form = ActiveForm::begin() ?>
                                <div class="my_appl_mdbody-1">
                                    <p>*<?= Yii::t('app','ф.и.о') ?></p>

                                    <?= $form->field($model, 'name',['options'=> ['style'=>'margin-left:12px']])->textInput(['maxlength' => true])->label(false) ?>


                                </div>
                                <div class="my_appl_mdbody-2">
                                    <p>*e-mail</p>
                                    <?= $form->field($model, 'email',['options'=> ['style'=>'margin-left:12px']])->textInput(['maxlength' => true])->label(false) ?>

                                </div>
                                <div class="my_appl_mdbody-3">
                                    <p>*<?= Yii::t('app','сообщение') ?></p>
                                    <?= $form->field($model, 'massege',['options'=> ['style'=>'margin-left:12px']])->textarea()->label(false) ?>

                                </div>
                                <div class="my_appl_mdbody-4">
                                    <button>отправить</button>
                                </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>





    <div class="container">
        <div class="footer_5icons">
            <div class="footer_5icons1">
                <?php if(!empty($services)): ?>
                    <?php foreach ( $services as $k => $item): ?>
                         <div class="footer_5icons_div1">
                            <div class="footer_5icons-img">
                                <img src="/uploads/<?= $item->path ?>">
                            </div>
                            <p><?= $item->title ?></p>
                         </div>
                        <?php if($k == 2): ?>
                            </div>
                            <div class="footer_5icons2">
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
        <!-- footer informatin -->
        <div class="footer_info">
            <div class="footer_info_1">
                <div class="info_11div">
                    <h6><?= Yii::t('app','ТЕЛЕФОН') ?></h6>
                    <p><?= $info->phone ?></p>
                </div>
                <div class="info_12div">
                    <h6><?= Yii::t('app','Email') ?></h6>
                    <p><?= $info->email ?></p>
                </div>
                <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"><?= Yii::t('app','оставить заявку') ?></button>
            </div>
            <div class="footer_info_23">
                <div class="footer_info_23_1">
                    <h6><?= Yii::t('app','ИНФОРМАЦИЯ') ?></h6>
                    <ul>
                        <?php if(!empty($pages)):  ?>
                            <?php foreach ($pages as $item): ?>
                                <li><a href="<?= Url::to(['/site/pages', 'id' => $item->id]) ?>"><?= $item->title ?></a></li>
                            <?php endforeach; ?>
                        <?php endif;  ?>
                    </ul>
                </div>
                <div class="footer_info_23_2">
                    <h6><?= Yii::t('app','КАТАЛОГ') ?></h6>
                    <ul>
                        <li><a href="<?= Url::to(['product/category','id' => 2]) ?>"><?= Yii::t('app','Для женщин') ?></a></li>
                        <li><a href="<?= Url::to(['product/category','id' => 1]) ?>"><?= Yii::t('app','Для мужчин') ?></a></li>
                        <li><a href="<?= Url::to(['product/category','id' => 3]) ?>"><?= Yii::t('app','Для детей') ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- logo botton -->
        <div class="bot_logoall">
            <div class="bot_logo_img">
                <img src="/img/logo_bot2.png">
            </div>
            <div class="bot_logo_p">
                <p>© 2018 <?= Yii::t('app','Все права защищены.')?></p>
            </div>
            <div class="bot_logo_sos">
                <p><?= Yii::t('app','Разработано в') ?> <span> <a href="http://sos.uz"><img src="/img/sos.png">  SOS</a> </span></p>
            </div>
        </div>
    </div>
    <div class="loader hidden"><img src="/images/2.gif"/></div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
