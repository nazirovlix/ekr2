<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 06.06.2018
 * Time: 15:41
 */

use app\models\Favorites;
use yii\helpers\Url;

?>
<div class="for_woman_8">
    <?php if(!empty($products)): ?>
        <?php foreach($products as $item): ?>
            <div class="for_woman-1div">
                <?php if(!empty($item->discount)):?>
                    <h5>-<?= $item->discount ?>%</h5>
                    <h3><?= number_format($item->discount_price,'0','.',' ') ?> <?= Yii::t('app','сум') ?></h3>
                    <h4><?= number_format($item->price,'0','.',' ') ?> <?= Yii::t('app','сум') ?></h4>
                <?php else: ?>
                    <h3><?= number_format($item->price,'0','.',' ') ?> <?= Yii::t('app','сум') ?></h3>
                <?php endif ?>

                <div class="for_woman-1div_imgd">
                    <a href="<?= Url::to(['/product/product','id' => $item->id]) ?>"><img src="/uploads/<?= $item->path ?>"></a>
                </div>
                <a href="<?= Url::to(['/product/product','id' => $item->id]) ?>"><h6><?= $item->title ?></h6></a>
                <div class="for_woman-1div_fev">
                    <p>#<?= $item->code ?></p>
                    <div class="woman-1div_fev_card">
                        <?php // Проверка сердечки
                        if(Yii::$app->user->isGuest):

                            $ff = 0;
                            if(!empty(Yii::$app->session['favorites'])){
                                $f = $_SESSION['favorites'];
                                foreach ($f as $fav):
                                    if($fav == $item->id){
                                        $ff = 1;
                                    }
                                endforeach;
                            }
                            if($ff == 1):  ?>
                                <div class="woman_like liked_items_main del-favorite" data-product="<?= $item->id ?>"></div>
                            <?php else: ?>
                                <div class="woman_like add-favorite" data-product="<?= $item->id ?>"></div>
                            <?php  endif;
                        else:
                            $favorites = Favorites::find()->where(['product_id'=> $item->id, 'user_id' => Yii::$app->user->id])->one();
                            if(!empty($favorites)){ ?>
                                <div class="woman_like liked_items_main del-favorite" data-product="<?= $item->id ?>"></div>
                            <?php } else{ ?>
                                <div class="woman_like add-favorite" data-product="<?= $item->id ?>"></div>
                            <?php    }
                        endif;
                        ?>
                    </div>
                </div>

            </div>
        <?php endforeach; ?>
    <?php else: ?>
        <h3 style="margin: 10px"><?= Yii::t('app','По вашему запросу ничего не найдено') ?></h3>
    <?php endif; ?>
</div>
