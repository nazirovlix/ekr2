<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 12.06.2018
 * Time: 16:51
 */
?>
<div class="ordering_content4-top">
    <?php if(!empty($order)): ?>
    <div class="ordering_content4-top_divs-first">
        <div class="ordering_content4-top-1">
            <h3><?= Yii::t('app','Личные данные') ?></h3>
            <div class="ordering_content4-top-12">
                <div class="ordering_content4-top-12-flex"><p><?= Yii::t('app','Ф.И.О') ?>:</p><span><?= $order->name ?></span>
                </div>
                <div class="ordering_content4-top-12-flex"><p><?= Yii::t('app','Тел') ?>.:</p><span><?= $order->phone ?></span>
                </div>
            </div>
            <a href="#" class="ordering_prev2"><?= Yii::t('app','Изменить') ?></a>
        </div>
        <div class="ordering_content4-top-2">
            <h3>Адрес доставки</h3>
            <div class="ordering_content4-top-13">
                <div class="ordering_content4-top-13-fix"><p><?= Yii::t('app','Регион') ?>:</p><span><?= $order->region->region->region; ?></span>
                </div>
                <div class="ordering_content4-top-13-fix"><p><?= Yii::t('app','Район') ?>:</p><span><?= $order->region->rayon?></span>
                </div>
                <div class="ordering_content4-top-13-fix"><p><?= Yii::t('app','Улица') ?>:</p><span><?= $order->street ?></span>
                </div>
                <div class="ordering_content4-top-13-fix"><p><?= Yii::t('app','Номер дома') ?>:</p><span><?= $order->number_dom ?></span>
                </div>
                <div class="ordering_content4-top-13-fix"><p><?= Yii::t('app','Номер кв.') ?>:</p><span><?= $order->number_kv ?></span>
                </div>
                <div class="ordering_content4-top-13-fix"><p><?= Yii::t('app','Индекс') ?></p><span><?= $order->index ?></span>
                </div>
            </div>
            <a href="#" class="ordering_prev2"><?= Yii::t('app','Изменить') ?></a>
        </div>
    </div>

    <div class="ordering_content4-top_divs-second">
        <div class="ordering_content4-top-3">
            <h3><?= Yii::t('app','Время доставки') ?></h3>
            <div class="ordering_content4-top-13">
                <p><?= Yii::t('app','Время удобное для вас') ?> <span><?= $order->time->time ?></span></p>
            </div>
            <a href="#" class="ordering_prev3"><?= Yii::t('app','Изменить') ?></a>
        </div>
        <div class="ordering_content4-top-4">
            <h3><?= Yii::t('app','Cпособ оплаты') ?></h3>
            <div class="ordering_content4-top-14">
                <p><?= $order->payment->name ?></p>
            </div>
            <a href="#" class="ordering_prev4"><?= Yii::t('app','Изменить') ?></a>
        </div>
    </div>
    <?php endif; ?>
</div>


<div class="ordering_content4-botton">
    <div class="bascet_goods_6divs">
        <div class="bascet_goods_6divs-1">
            <div class="bascet_goods_6-1">
                <p>№</p>
            </div>
            <div class="bascet_goods_6-2">
                <p><?= Yii::t('app','ПРОДУКЦИЯ') ?></p>
            </div>
            <div class="bascet_goods_6-3">
                <p><?= Yii::t('app','О ПРОДУКЦИИ') ?></p>
            </div>
            <div class="bascet_goods_6-4">
                <p><?= Yii::t('app','ЦЕНА') ?></p>
            </div>
            <div class="bascet_goods_6-5">
                <p><?= Yii::t('app','КОЛ-ВО') ?></p>
            </div>
            <div class="bascet_goods_6-6">
                <p><?= Yii::t('app','ИТОГО') ?></p>
            </div>
        </div>
    <?php if(!empty($session)): ?>
        <?php $k =1 ;?>
        <?php foreach ($session as $cart): ?>
          <div class="bascet_goods_6divs-2">
            <div class="bascet_goods_62-1">
                <p><?= $k++ ?></p>
            </div>
            <div class="bascet_goods_62-2">
                <div class="bascet_goods_62-2_img">
                    <img src="/uploads/<?= $cart['img'] ?>">
                </div>
            </div>
            <div class="bascet_goods_62-3">
                <div class="bascet_goods_62-3_all">

                    <h6><?= $cart['name'] ?></h6>
                    <p>#<?= $cart['code'] ?></p>
                    <div class="bascet_goods_62-3_wo">
                        <h4><?= Yii::t('app','РАЗМЕР') ?>:</h4>
                        <h3><?= $cart['size'] ?></h3>
                    </div>
                    <div class="bascet_goods_62-3_wo">
                        <h4><?= Yii::t('app','ЦВЕТ') ?>:</h4>
                        <h3><?= $cart['color'] ?></h3>
                    </div>
                </div>
            </div>
            <div class="bascet_goods_62-4">
                <p><?=$cart['color']?> <?= Yii::t('app','сум') ?></p>
            </div>
            <div class="bascet_goods_62-5">
                <span><?= $cart['qty'] ?></span>
            </div>
            <div class="bascet_goods_62-6">
                <p><?= number_format($_SESSION['cart.sum'], '0','.',' ') ?> <?= Yii::t('app','сум') ?></p>
            </div>
        </div>
        <?php $k++;?>
        <?php endforeach; ?>

        <div class="bascet_goods_bot-h3-li">
            <h3><?= Yii::t('app','СУММА К ОПЛАТЕ') ?>: <?= number_format($_SESSION['cart.sum'], '0','.',' ') ?> <?= Yii::t('app','СУМ') ?></h3>
        </div>
    <?php endif; ?>
    </div>
</div>
