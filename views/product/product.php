<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 05.06.2018
 * Time: 15:26
 */

use yii\helpers\Url;

?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<!-- breakpoint -->
<section class="breakpoint">
    <div class="container">
        <div class="breakpoint_all">
            <div class="slider_break">
            <?php if(!empty($product)): ?>
                <div class="slick_break">
                    <div class="top-left-ab-pr-bt">
                        <img id="zoom_03" src="/uploads/<?= $product->path ?>" data-zoom-image="/uploads/<?= $product->path ?>"/>
                    </div>
                </div>
                <?php endif; ?>
            </div>
            <div class="break_sale">

                <input class="hidden" value="<?= Yii::t('app', 'Ваш продукт добавлен в корзину!') ?>" id="swal1">
                <?php if (Yii::$app->session->hasFlash('add')): ?>
                    <?php $this->registerJs('
                                var swal_a = $("#swal1").val();                              
                                swal(swal_a," ", "success");
                                '); ?>
                <?php endif; ?>
                <div class="break_sale_con">
                    <?php if(!empty($product->discount)):  ?>
                      <h5>-<?= $product->discount ?>%</h5>
                    <?php endif ?>
                    <h6><?= $product->title ?> </h6>
                    <p>#<?= $product->code ?></p>
                </div>
                <div class="break_sale_sec">
                    <?php if(!empty($product->discount)):  ?>
                        <h2><?= number_format($product->discount_price,'0','.',' ') ?> <?= Yii::t('app','сум')?></h2>
                        <h3><?= number_format($product->price,'0','.',' ') ?> <?= Yii::t('app','сум')?></h3>
                    <?php else: ?>
                        <h2><?= number_format($product->price,'0','.',' ') ?> <?= Yii::t('app','сум')?></h2>
                    <?php endif ?>
                    <p><?= Yii::t('app','Цвет') ?></p>
                    <form action="<?= Url::to(['product/add-to-cart']) ?>" method="get">


                        <?php if(!empty($id_product)): ?>
                            <input type="hidden" value="<?= $id_product ?>" name="id">
                        <?php else: ?>
                            <input type="hidden" name="id" value="<?= $product->id ?>">
                        <?php endif; ?>
                    <div class="break_sale_option">
                        <select name="color">

                           <?php foreach ($product->colorss as $item): ?>
                               <option><?= $item->color->title  ?></option>
                           <?php endforeach; ?>

                        </select>
                        <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                    </div>
                    <p><?= Yii::t('app','размер') ?></p>
                    <div class="break_sale_option">
                        <select name="size">
                            <?php foreach ($product->sizess as $item): ?>
                                <option><?= $item->size->size?></option>
                            <?php endforeach; ?>
                        </select>
                        <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                    </div>
                    <button class="active_for_card" type="submit"><?= Yii::t('app','ДОБАВИТЬ В КОРЗИНУ') ?><span><i class="fa fa-shopping-cart" aria-hidden="true"></i></span></button>

                    <button class="add-tofavorites" data-product="<?= $product->id ?>"><?= Yii::t('app','В СПИСОК ИЗБРАННЫХ') ?> <span><i class="fa fa-heart" aria-hidden="true"></i></span></button>
                    <button type="button"  data-toggle="modal" data-target="#exampleModalCenter"  class="sp-po-bton btn btn-primary" href="#"><?= Yii::t('app','Справочник по размерам') ?> </button>
                    </form>
                </div>
            </div>
        </div>
        <div class="slider_4div_btn">
            <button type="button" class="slick-preva"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
            <button type="button" class="slick-nexta"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
            <div class="left-ab-pr-bt">

                <div id="gallery_01">
                    <a href="#" class="active" data-image="/uploads/<?= $product->path ?>" data-zoom-image="/uploads/<?= $product->path ?>">
                        <img id="/img_01" src="/uploads/<?= $product->path ?>"/>
                    </a>
                    <?php if(!empty($product->pimages)):?>
                    <?php foreach ($product->pimages as $item):  ?>
                        <a href="#" class="active" data-image="/uploads/<?= $item->path ?>" data-zoom-image="/uploads/<?= $item->path ?>">
                            <img id="/img_01" src="/uploads/<?= $item->path ?>"/>
                        </a>
                    <?php endforeach; ?>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Suede Blue -->
<section class="suede">
    <div class="container">
        <div class="suede_all">
            <h3><?= $product->title ?></h3>
          <p><?= $product->content ?></p>
        </div>
    </div>
</section>

<!-- for woman -->
<section class="for_woman">
    <div class="container">
        <div class="suede_all_h4">
            <h4><?= Yii::t('app','клиенты так же покупали') ?></h4>
        </div>
        <div class="for_woman_8">


            <?php if(!empty($most)): ?>
                <?php foreach($most as $item): ?>
                    <div class="for_woman-1div">
                        <?php if(!empty($item->discount)):?>
                            <h5>-<?= $item->discount ?>%</h5>
                            <h3><?= number_format($item->discount_price,'0','.',' ') ?> <?= Yii::t('app','сум') ?></h3>
                            <h4><?= number_format($item->price,'0','.',' ') ?> <?= Yii::t('app','сум') ?></h4>
                        <?php else: ?>
                            <h3><?= number_format($item->price,'0','.',' ') ?> <?= Yii::t('app','сум') ?></h3>
                        <?php endif ?>

                        <div class="for_woman-1div_imgd">
                            <a href="<?= Url::to(['/product/product','id' => $item->id]) ?>"><img src="/uploads/<?= $item->path ?>"></a>
                        </div>
                        <a href="<?= Url::to(['/product/product','id' => $item->id]) ?>"><h6><?= $item->title ?></h6></a>
                        <div class="for_woman-1div_fev">
                            <p>#<?= $item->code ?></p>
                            <div class="woman-1div_fev_card">
                                <div class="woman_like"></div>
                                <div class="woman_card"></div>
                            </div>
                        </div>

                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="my-reference">
    <div class="modal fade reference-moddal" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content my-reference-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Справочник по размерам</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="reference-mod_body">



                        <button class="accordion">ДЛЯ МУЖЧИН</button>
                        <div class="panel">
                            <div class="reference-mod_acar-panel1">
                                <div class="reference-mod-start"></div>
                                <table class="reference-fr-mn">
                                    <tr>
                                        <th>Стандартный размер</th>
                                        <th>39</th>
                                        <th>39,5</th>
                                        <th>40</th>
                                        <th>40,5</th>
                                        <th>41</th>
                                        <th>41,5</th>
                                        <th>42</th>
                                        <th>42,5</th>
                                        <th>43</th>
                                        <th>43,5</th>
                                        <th>44</th>
                                        <th>45</th>
                                        <th>46</th>
                                    </tr>
                                    <tr>
                                        <td>Американский размер</td>
                                        <td>7</td>
                                        <td>7,5</td>
                                        <td>8</td>
                                        <td>8,5</td>
                                        <td>9</td>
                                        <td>9,5</td>
                                        <td>10</td>
                                        <td>10,5</td>
                                        <td>11</td>
                                        <td>11,5</td>
                                        <td>12</td>
                                        <td>13</td>
                                        <td>14</td>
                                    </tr>
                                    <tr class="reference-fr-mn-bc">
                                        <td>Европейский размер</td>
                                        <td>40</td>
                                        <td>40,5</td>
                                        <td>41</td>
                                        <td>41,5</td>
                                        <td>42</td>
                                        <td>42,5</td>
                                        <td>43</td>
                                        <td>43,5</td>
                                        <td>44</td>
                                        <td>44,5</td>
                                        <td>45</td>
                                        <td>46</td>
                                        <td>47</td>
                                    </tr>
                                    <tr>
                                        <td>В сантиметрах</td>
                                        <td>25</td>
                                        <td>25,5</td>
                                        <td>26</td>
                                        <td>26,5</td>
                                        <td>27</td>
                                        <td>27,5</td>
                                        <td>28</td>
                                        <td>28,5</td>
                                        <td>29</td>
                                        <td>29,5</td>
                                        <td>30</td>
                                        <td>31</td>
                                        <td>32</td>
                                    </tr>
                                </table>

                                <div class="reference-fr-mn-3img">
                                    <img src="/img/step.png">
                                </div>
                            </div>
                        </div>

                        <button class="accordion">ДЛЯ ЖЕНЩИН</button>
                        <div class="panel">


                            <div class="reference-mod_acar-panel1">
                                <div class="reference-mod-start"></div>
                                <table class="reference-fr-mn">
                                    <tr>
                                        <th>Стандартный размер</th>
                                        <th>34</th>
                                        <th>34,5</th>
                                        <th>35</th>
                                        <th>35,5</th>
                                        <th>36</th>
                                        <th>36,5</th>
                                        <th>37</th>
                                        <th>37,5</th>
                                        <th>38</th>
                                        <th>38,5</th>
                                        <th>39</th>
                                    </tr>
                                    <tr>
                                        <td>Американский размер</td>
                                        <td>5</td>
                                        <td>5,5</td>
                                        <td>6</td>
                                        <td>6,5</td>
                                        <td>7</td>
                                        <td>7,5</td>
                                        <td>8</td>
                                        <td>8,5</td>
                                        <td>9</td>
                                        <td>9,5</td>
                                        <td>10</td>

                                    </tr>
                                    <tr class="reference-fr-mn-bc">
                                        <td>Европейский размер</td>
                                        <td>35</td>
                                        <td>35,5</td>
                                        <td>36</td>
                                        <td>36,5</td>
                                        <td>37</td>
                                        <td>37,5</td>
                                        <td>38</td>
                                        <td>38,5</td>
                                        <td>39</td>
                                        <td>39,5</td>
                                        <td>40</td>
                                    </tr>
                                    <tr>
                                        <td>В сантиметрах</td>
                                        <td>21,5</td>
                                        <td>22</td>
                                        <td>22,5</td>
                                        <td>23</td>
                                        <td>23,5</td>
                                        <td>24</td>
                                        <td>24,5</td>
                                        <td>25</td>
                                        <td>25,5</td>
                                        <td>26</td>
                                        <td>26,5</td>
                                    </tr>
                                </table>

                                <div class="reference-fr-mn-3img">
                                    <img src="/img/step.png">
                                </div>
                            </div>



                        </div>

                        <button class="accordion">ДЛЯ ДЕТЕЙ</button>
                        <div class="panel">

                            <div class="reference-mod_acar-panel1">
                                <div class="reference-mod-start"></div>
                                <table class="reference-fr-mn reference-for-choldren">
                                    <tr>
                                        <th>Возвраст</th>
                                        <th>В сантиметрах</th>
                                        <th>Стандартный размер</th>
                                        <th>Американский размер</th>
                                        <th>Европейский размер</th>
                                    </tr>
                                    <tr>
                                        <td>От 0 до 3 месяцев</td>
                                        <td>9,5</td>
                                        <td>16</td>
                                        <td>1</td>
                                        <td>16</td>
                                    </tr>
                                    <tr class="reference-fr-mn-bc  refernce-whiteba">
                                        <td rowspan="2">От 3 до 6 месяцев</td>
                                        <td>10</td>
                                        <td>16,5</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr class="reference-fr-mn-bc  refernce-whiteba">

                                        <td class="reference-for-choldren-leftst">10,5</td>
                                        <td>17</td>
                                        <td>2</td>
                                        <td>17</td>
                                    </tr>
                                    <tr class="reference-fr-mn-bc">
                                        <td rowspan="2">От 3 до 6 месяцев</td>
                                        <td>11</td>
                                        <td>18</td>
                                        <td>3</td>
                                        <td>18</td>
                                    </tr>
                                    <tr class="reference-fr-mn-bc">

                                        <td class="reference-for-choldren-leftst">11,5</td>
                                        <td>19</td>
                                        <td>4</td>
                                        <td>19</td>
                                    </tr>
                                    <tr class="reference-fr-mn-bc  refernce-whiteba">
                                        <td>От 9 до 12 месяцев</td>
                                        <td>12</td>
                                        <td>19,5</td>
                                        <td>5</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td>1 год</td>
                                        <td>12,5</td>
                                        <td>20</td>
                                        <td>-</td>
                                        <td>20</td>
                                    </tr>
                                    <tr class="reference-fr-mn-bc  refernce-whiteba">
                                        <td>От 1 до 1,3 года</td>
                                        <td>13</td>
                                        <td>21</td>
                                        <td>6</td>
                                        <td>21</td>
                                    </tr>
                                    <tr class="reference-fr-mn-bc">
                                        <td rowspan="2">1,5 года</td>
                                        <td>13</td>
                                        <td>22</td>
                                        <td>-</td>
                                        <td>22</td>
                                    </tr>
                                    <tr class="reference-fr-mn-bc">

                                        <td class="reference-for-choldren-leftst">14</td>
                                        <td>22,5</td>
                                        <td>7</td>
                                        <td>-</td>
                                    </tr>
                                    <tr class="reference-fr-mn-bc  refernce-whiteba">
                                        <td rowspan="2">2 года</td>
                                        <td>14,5</td>
                                        <td>23</td>
                                        <td>-</td>
                                        <td>23</td>
                                    </tr>
                                    <tr class="reference-fr-mn-bc  refernce-whiteba">

                                        <td class="reference-for-choldren-leftst">15</td>
                                        <td>24</td>
                                        <td>8</td>
                                        <td>24</td>
                                    </tr>
                                    <tr>
                                        <td>3 года</td>
                                        <td>15,5</td>
                                        <td>25</td>
                                        <td>9</td>
                                        <td>25</td>
                                    </tr>
                                    <tr class="reference-fr-mn-bc  refernce-whiteba">
                                        <td rowspan="3">От 4 до 5 лет</td>
                                        <td>16</td>
                                        <td>26</td>
                                        <td>10</td>
                                        <td>26</td>
                                    </tr>
                                    <tr class="reference-fr-mn-bc  refernce-whiteba">

                                        <td class="reference-for-choldren-leftst">17</td>
                                        <td>27</td>
                                        <td>-</td>
                                        <td>27</td>
                                    </tr>
                                    <tr class="reference-fr-mn-bc  refernce-whiteba">

                                        <td class="reference-for-choldren-leftst">17,5</td>
                                        <td>28</td>
                                        <td>11</td>
                                        <td>28</td>
                                    </tr>
                                    <tr class="reference-fr-mn-bc">
                                        <td rowspan="3">От 5 до 6 лет</td>
                                        <td>18</td>
                                        <td>28,5</td>
                                        <td>12</td>
                                        <td>-</td>
                                    </tr>
                                    <tr class="reference-fr-mn-bc">

                                        <td class="reference-for-choldren-leftst">18,5</td>
                                        <td>29</td>
                                        <td>-</td>
                                        <td>29</td>
                                    </tr>
                                    <tr class="reference-fr-mn-bc">

                                        <td class="reference-for-choldren-leftst">19</td>
                                        <td>30</td>
                                        <td>13</td>
                                        <td>30</td>
                                    </tr>
                                    <tr class="reference-fr-mn-bc  refernce-whiteba">
                                        <td rowspan="3">От 7 до 8 лет</td>
                                        <td>19</td>
                                        <td>31</td>
                                        <td>-</td>
                                        <td>31</td>
                                    </tr>
                                    <tr class="reference-fr-mn-bc  refernce-whiteba">

                                        <td class="reference-for-choldren-leftst">20</td>
                                        <td>31,5</td>
                                        <td>1</td>
                                        <td>-</td>
                                    </tr>
                                    <tr class="reference-fr-mn-bc  refernce-whiteba">

                                        <td class="reference-for-choldren-leftst">20,5</td>
                                        <td>32</td>
                                        <td>2</td>
                                        <td>32</td>
                                    </tr>
                                    <tr class="reference-fr-mn-bc">
                                        <td rowspan="5">От 8 лет</td>
                                        <td>21</td>
                                        <td>33</td>
                                        <td>-</td>
                                        <td>33</td>
                                    </tr>
                                    <tr class="reference-fr-mn-bc">

                                        <td class="reference-for-choldren-leftst">21,5</td>
                                        <td>34</td>
                                        <td>3</td>
                                        <td>34<td>
                                    </tr>
                                    <tr class="reference-fr-mn-bc">

                                        <td class="reference-for-choldren-leftst">22</td>
                                        <td>33,5</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <td class="reference-for-choldren-leftst">22,5</td>
                                    <td>35</td>
                                    <td>4</td>
                                    <td>35</td>
                                    </tr>
                                    <td class="reference-for-choldren-leftst">23</td>
                                    <td>36</td>
                                    <td>5</td>
                                    <td>36</td>
                                    </tr>
                                </table>

                                <div class="reference-fr-mn-3img">
                                    <img src="/img/step.png">
                                </div>
                            </div>



                        </div>






                    </div>
                </div>
            </div>
        </div>
    </div>
</div>