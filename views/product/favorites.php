<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 07.06.2018
 * Time: 15:28
 */

use yii\helpers\Url;

?>
<?php if(!empty($favorites)): ?>
<section class="favorite_pro">
    <div class="container">
        <div class="favorite_pro_all favorite_free_all">
            <div class="favorite_free-h1">

                <h1><?= Yii::t('app','Избранные') ?></h1>
            </div>

            <div class="for_woman_8">
                <?php foreach ($favorites as $item): ?>
                <div class="for_woman-1div_pro">
                    <div class="for_woman-1div">
                        <button class="for_woman-1div_disap del-favorite" data-product="<?= $item->id ?>"><img src="/img/x.png" class=""></button>
                    <?php if(!empty($item->discount)):?>
                        <h3><?= number_format($item->discount_price,'0','.',' ') ?> <?= Yii::t('app','сум') ?></h3>
                        <h5>-<?= $item->discount ?>%</h5>
                        <h4><?= number_format($item->price,'0','.',' ') ?> <?= Yii::t('app','сум') ?></h4>
                    <?php else: ?>
                        <h3><?= number_format($item->price,'0','.',' ') ?> <?= Yii::t('app','сум') ?></h3>
                    <?php endif ?>
                        <div class="for_woman-1div_imgd">
                            <a href="<?= Url::to(['/product/product','id' => $item->id]) ?>"><img src="/uploads/<?= $item->path ?>"></a>
                        </div>
                        <a href="<?= Url::to(['/product/product','id' => $item->id]) ?>"><h6><?= $item->title ?></h6></a>
                        <div class="for_woman-1div_fev">
                            <p>#<?= $item->code ?></p>
                        </div>
                    </div>
                    <button  type="button" data-toggle="modal" data-target="#myModal_<?=$item->id ?>" class="btn btn-info btn-lg for_woman-1div_ap" ><?= Yii::t('app','Добавить в корзину')?></button>
                </div>





                    <div class="allmymodal">
                        <div class="myall_modal_in modal fade" id="myModal_<?= $item->id ?>" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title"><?= Yii::t('app','Быстрый просмотр') ?> </h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="my_lnmodal-cons">
                                            <div class="my_lnmodal-cons_top">
                                                <div class="cons_top_slider1_cover">
                                                    <div class="cons_top_slider1">
                                                        <div class="cons_top_slider1_div"><img src="/uploads/<?=$item->path ?>"></div>
                                                        <?php if(!empty($item->pimages)):?>
                                                        <?php foreach ($item->pimages as $pic):  ?>
                                                        <div class="cons_top_slider1_div"><img src="/uploads/<?=$pic->path ?>"></div>
                                                            <?php endforeach; ?>
                                                        <?php endif;?>
                                                    </div>
                                                </div>
                                                <div class="break_sale">
                                                    <input class="hidden" value="<?= Yii::t('app', 'Ваш продукт добавлен в корзину!') ?>" id="swal1">
                                                    <?php if (Yii::$app->session->hasFlash('add')): ?>
                                                        <?php $this->registerJs('
                                                    var swal_a = $("#swal1").val();                              
                                                    swal(swal_a," ", "success");
                                                    '); ?>
                                                        <?php endif; ?>
                                                    <div class="break_sale_con">
                                                        <?php if(!empty($item->discount)):  ?>
                                                            <h5>-<?= $item->discount ?>%</h5>
                                                        <?php endif ?>
                                                        <h6><?= $item->title ?> </h6>
                                                        <p>#<?= $item->code ?></p>
                                                    </div>
                                                    <div class="break_sale_sec">
                                                        <?php if(!empty($item->discount)):  ?>
                                                            <h2><?= number_format($item->discount_price,'0','.',' ') ?> <?= Yii::t('app','сум')?></h2>
                                                            <h3><?= number_format($item->price,'0','.',' ') ?> <?= Yii::t('app','сум')?></h3>
                                                        <?php else: ?>
                                                            <h2><?= number_format($item->price,'0','.',' ') ?> <?= Yii::t('app','сум')?></h2>
                                                        <?php endif ?>
                                                        <p><?= Yii::t('app','Цвет') ?></p>
                                                        <form action="<?= Url::to(['product/add-to-cart']) ?>" method="get">
                                                            <input type="hidden" name="id" value="<?=$item->id ?>">
                                                        <div class="break_sale_option">
                                                            <select name="color">

                                                                <?php foreach ($item->colorss as $col): ?>
                                                                    <option><?= $col->color->title  ?></option>
                                                                <?php endforeach; ?>

                                                            </select>
                                                            <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                                                        </div>
                                                        <p><?= Yii::t('app','размер') ?></p>
                                                        <div class="break_sale_option">
                                                            <select name="name">
                                                                <?php foreach ($item->sizess as $siz): ?>
                                                                    <option><?= $siz->size->size?></option>
                                                                <?php endforeach; ?>
                                                            </select>
                                                            <span><i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                                                        </div>
                                                        <button class="active_for_card" type="submit"><?= Yii::t('app','ДОБАВИТЬ В КОРЗИНУ') ?> <span><i class="fa fa-shopping-cart" aria-hidden="true"></i></span></button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="all4_with-btn">
                                                <button type="button" class="slick-prevp"><i class="fa fa-angle-left" aria-hidden="true"></i></button>
                                                <button type="button" class="slick-nextp"><i class="fa fa-angle-right" aria-hidden="true"></i></button>
                                                <div class="all4_slider_bot">

                                                        <div class="all4_slider_bot_1"><img src="/uploads/<?= $item->path ?>"></div>
                                                    <?php if(!empty($item->pimages)):?>
                                                    <?php foreach ($item->pimages as $pics):  ?>
                                                        <div class="all4_slider_bot_1"><img src="/uploads/<?= $pics->path ?>"></div>
                                                        <?php endforeach; ?>
                                                    <?php endif;?>


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>




















            <?php endforeach; ?>
            </div>

            <div class="paganation_all">
                    <div class="paganation_div">
                        <div class="pagination">
                            <?php
                            echo \yii\widgets\LinkPager::widget([
                                'pagination' => $pages,
                                'options' => ['class' => false],
                                'prevPageLabel' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                                'nextPageLabel' => '<i class="fa fa-angle-right" aria-hidden="true"></i>',
                                'activePageCssClass' => 'activeLi',

                            ]); ?>
                        </div>
                    </div>
            </div>

        </div>
    </div>
</section>

<?php else: ?>
    <section class="favorite_free_body">
        <div class="container">
            <div class="favorite_free_all">
                <div class="favorite_free-h1">
                    <h1><?= Yii::t('app','Избранные') ?> <span>(0)</span></h1>
                </div>
                <div class="favorite_free-no">
                    <p><?= Yii::t('app','Вы еще не добавили избранные товары') ?></p>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

