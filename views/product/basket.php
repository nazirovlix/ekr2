<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 09.06.2018
 * Time: 14:07
 */

use yii\helpers\Url;

?>
<!-- bascet_goods -->
<section class="bascet_goods">
    <div class="container">
        <div class="bascet_goods_all_h4">
            <div class="bascet_goods_all_h4_topp"><h4><?= Yii::t('app','КОРЗИНА') ?></h4></div>
            <div class="bascet_goods_6divs">
                <div class="bascet_goods_6divs-1">
                    <div class="bascet_goods_6-1">
                        <p>№</p>
                    </div>
                    <div class="bascet_goods_6-2">
                        <p><?= Yii::t('app','ПРОДУКЦИЯ') ?></p>
                    </div>
                    <div class="bascet_goods_6-3">
                        <p><?= Yii::t('app','О ПРОДУКЦИИ') ?></p>
                    </div>
                    <div class="bascet_goods_6-4">
                        <p><?= Yii::t('app','ЦЕНА') ?></p>
                    </div>
                    <div class="bascet_goods_6-5">
                        <p><?= Yii::t('app','КОЛ-ВО') ?></p>
                    </div>
                    <div class="bascet_goods_6-6">
                        <p><?= Yii::t('app','ИТОГО') ?></p>
                    </div>
                </div>

                <?php if(!empty($session)): ?>
                <?php foreach ($session as $k => $item): ?>
                    <div class="bascet_goods_6divs-2">
                    <div class="bascet_goods_62-1">
                        <p><?= $k+1 ?></p>
                    </div>
                    <div class="bascet_goods_62-2">
                        <div class="bascet_goods_62-2_img">
                            <img src="/uploads/<?= $item['img']?>">
                        </div>
                    </div>
                    <div class="bascet_goods_62-3">
                        <div class="bascet_goods_62-3_all">
                            <h6><?= $item['name'] ?></h6>
                            <p>#<?= $item['code'] ?></p>
                            <div class="bascet_goods_62-3_wo">
                                <h4><?= Yii::t('app','РАЗМЕР') ?>:</h4>
                                <h3><?= $item['size'] ?></h3>
                            </div>
                            <div class="bascet_goods_62-3_wo">
                                <h4><?= Yii::t('app','ЦВЕТ') ?>:</h4>
                                <h3><?= $item['color'] ?></h3>
                            </div>
                            <a href="<?= Url::to(['/product/change','id' => $k]) ?>"><?= Yii::t('app','ИЗМЕНИТЬ') ?></a>
                        </div>
                    </div>
                    <div class="bascet_goods_62-4">
                        <p><?= number_format($item['price'],'0','.', ' ') ?> <?= Yii::t('app','сум') ?></p>
                    </div>
                    <div class="bascet_goods_62-5">
                        <div class="prod_rew_amt_ctrl">
                            <button class="prod_rew_amt_ctrl_btn minus" type="button" data-id="<?= $k ?>">-</button>
                            <input type="text" class="prod_rew_amt_ctrl_inp" value="<?= $item['qty'] ?>">
                            <button class="prod_rew_amt_ctrl_btn plus" type="button" data-id="<?= $k ?>">+</button>
                        </div>
                    </div>
                    <div class="bascet_goods_62-6">
                        <p><?= number_format($item['price']*$item['qty'],'0','.', ' ') ?> <?= Yii::t('app','сум') ?></p>
                        <a href="#" class="del-cart" data-id="<?= $k ?>"><?= Yii::t('app','УДАЛИТЬ') ?></a>
                    </div>
                </div>
                <?php endforeach; ?>


                <div class="bascet_goods_bot-h3-li" style="justify-content: space-between">
                    <a href="<?= Url::to(['/product/remove-cart']) ?>"><?= Yii::t('app','Очистить корзину') ?></a>

                    <h3><?= Yii::t('app','СУММА К ОПЛАТЕ') ?>: <?= number_format($_SESSION['cart.sum'],'0','.',' ') ?> <?= Yii::t('app','СУМ') ?></h3>


                </div>

                <div class="bascet_goods_bot-last_btn">
                    <?php if(Yii::$app->user->isGuest): ?>
                    <div class="bascet_goods_bot-last_btn_il">
                        <a href="<?= Url::to(['/site/login']) ?>"><?= Yii::t('app','ВОЙТИ') ?></a>
                        <p><?= Yii::t('app','ИЛИ') ?></p>
                        <a href="<?= Url::to(['/site/order']) ?>"><?= Yii::t('app','КУПИТЬ КАК ГОСТЬ') ?></a>
                    </div>
                    <?php else: ?>
                        <div class="bascet_goods_bot-last_btn">
                            <a ><?= Yii::t('app','ОФОРМИТЬ ЗАКАЗ') ?></a>
                        </div>
                    <?php endif; ?>
                </div>
                <?php else: ?>
                    <div class="bascet_goods_bot-h3-li" style="justify-content: space-around">
                    <h3 style="margin-bottom: 50px"><?= Yii::t('app','Корзина пуста') ?></h3>
                    </div>
                <?php endif ?>
            </div>
        </div>
    </div>
</section>

<!-- for woman -->
<section class="for_woman">
    <div class="container">
        <div class="suede_all_h4">
            <h4><?= Yii::t('app','клиенты так же покупали') ?></h4>
        </div>
        <div class="for_woman_8">


            <?php if(!empty($most)): ?>
                <?php foreach($most as $item): ?>
                    <div class="for_woman-1div">
                        <?php if(!empty($item->discount)):?>
                            <h5>-<?= $item->discount ?>%</h5>
                            <h3><?= number_format($item->discount_price,'0','.',' ') ?> <?= Yii::t('app','сум') ?></h3>
                            <h4><?= number_format($item->price,'0','.',' ') ?> <?= Yii::t('app','сум') ?></h4>
                        <?php else: ?>
                            <h3><?= number_format($item->price,'0','.',' ') ?> <?= Yii::t('app','сум') ?></h3>
                        <?php endif ?>

                        <div class="for_woman-1div_imgd">
                            <a href="<?= Url::to(['/product/product','id' => $item->id]) ?>"><img src="/uploads/<?= $item->path ?>"></a>
                        </div>
                        <a href="<?= Url::to(['/product/product','id' => $item->id]) ?>"><h6><?= $item->title ?></h6></a>
                        <div class="for_woman-1div_fev">
                            <p>#<?= $item->code ?></p>
                            <div class="woman-1div_fev_card">
                                <div class="woman_like"></div>
                                <div class="woman_card"></div>
                            </div>
                        </div>

                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</section>
