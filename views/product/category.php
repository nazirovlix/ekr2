<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 05.06.2018
 * Time: 13:53
 */

use app\models\Favorites;
use yii\helpers\Url;

?>


<section class="filtr_top">
    <div class="container">
        <div class="filtr_top_backimg" style="background-image: url(/uploads/<?= $category->path ?>)">
            <div class="filtr_top_backimg_h1">
                <h1><?= $category->title ?></h1>
            </div>
        </div>
    </div>
</section>

<!-- price size -->
<section class="price_size">
    <div class="container">
        <div class="price_size_2div">
            <div class="price_size_left">


                <form id="filter-category" action="<?= Url::to(['product/category-filter']) ?>" method="get">
                    <input type="hidden" name="id" value="<?= Yii::$app->request->get('id') ?>">
                    <span class="accordionv1"><?= Yii::t('app', 'ЦЕНА') ?></span>
                    <div class="panelv1">
                        <div class="panel-od">
                            <?php foreach ($prices as $k => $item): ?>
                                <div class="checkbox2">
                                    <input id="prc<?= $k + 1 ?>" type="radio" name="price" value="<?= $item->id ?>">
                                    <label for="prc<?= $k + 1 ?>" class="checkbox_label prace_lab">
                                        <?= Yii::t('app', 'от') ?>
                                        <?= number_format($item->from, '0', '.', ' ') ?>
                                        <?php if ($item->to == null): ?>

                                            <?= Yii::t('app', 'и более') ?>
                                        <?php else: ?>
                                            -
                                            <?= Yii::t('app', 'до') ?>
                                            <?= number_format($item->to, '0', '.', ' ') ?>
                                        <?php endif; ?>
                                    </label>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>

                    <span class="accordionv1"><?= Yii::t('app', 'РАЗМЕР') ?></span>
                    <div class="panelv1">
                        <div class="panel-od">
                            <?php if (!empty($sizes)): ?>
                                <?php foreach ($sizes as $k => $item): ?>
                                    <div class="checkbox2">
                                        <input id="size<?= $k + 1 ?>" type="checkbox" name="size[]"
                                               value="<?= $item->id ?>">
                                        <label for="size<?= $k + 1 ?>"
                                               class="checkbox_label"><?= $item->size ?>
                                        </label>
                                    </div>
                                <?php endforeach; ?>
                            <?php endif; ?>

                        </div>
                    </div>

                    <span class="accordionv1"><?= Yii::t('app', 'СТРАНА ПРОИСХОЖДЕНИЯ') ?></span>
                    <div class="panelv1">
                        <div class="panel-od">
                            <?php if (!empty($country)): ?>
                                <?php foreach ($country as $k => $item) { ?>
                                    <div class="checkbox2">
                                        <input id="countr<?= $k + 1 ?>" type="checkbox" name="country[]"
                                               value="<?= $item->id ?>">
                                        <label for="countr<?= $k + 1 ?>"
                                               class="checkbox_label"><?= $item->title ?>
                                        </label>
                                    </div>
                                <?php } ?>
                            <?php endif; ?>

                        </div>
                    </div>

                    <span class="accordionv1"><?= Yii::t('app', 'БРЕНД') ?></span>
                    <div class="panelv1">
                        <div class="panel-od">
                            <?php foreach ($brands as $k => $item): ?>
                                <div class="checkbox2">
                                    <input id="brn<?= $k + 1 ?>" type="checkbox" name="brand[]"
                                           value="<?= $item->id ?>">
                                    <label for="brn<?= $k + 1 ?>"
                                           class="checkbox_label"><?= $item->name ?>
                                    </label>
                                </div>
                            <?php endforeach; ?>

                        </div>
                    </div>
                </form>

            </div>
            <div class="price_size_right for_woman products_filter">
                <div class="for_woman_8">
                    <?php if (!empty($products)): ?>
                        <?php foreach ($products as $item): ?>
                            <div class="for_woman-1div">
                                <?php if (!empty($item->discount)): ?>
                                    <h5>-<?= $item->discount ?>%</h5>
                                    <h3><?= number_format($item->discount_price, '0', '.', ' ') ?> <?= Yii::t('app', 'сум') ?></h3>
                                    <h4><?= number_format($item->price, '0', '.', ' ') ?> <?= Yii::t('app', 'сум') ?></h4>
                                <?php else: ?>
                                    <h3><?= number_format($item->price, '0', '.', ' ') ?> <?= Yii::t('app', 'сум') ?></h3>
                                <?php endif ?>

                                <div class="for_woman-1div_imgd">
                                    <a href="<?= Url::to(['/product/product', 'id' => $item->id]) ?>"><img
                                                src="/uploads/<?= $item->path ?>"></a>
                                </div>
                                <a href="<?= Url::to(['/product/product', 'id' => $item->id]) ?>">
                                    <h6><?= $item->title ?></h6></a>
                                <div class="for_woman-1div_fev">
                                    <p>#<?= $item->code ?></p>
                                    <div class="woman-1div_fev_card">
                                        <?php // Проверка сердечки
                                        if(Yii::$app->user->isGuest):
                                           $ff = 0;
                                            if(!empty(Yii::$app->session['favorites'])){
                                                $f = $_SESSION['favorites'];
                                                foreach ($f as $fav):
                                                    if($fav == $item->id){
                                                        $ff = 1;
                                                    }
                                                endforeach;
                                            }
                                            if($ff == 1):  ?>
                                                <div class="woman_like liked_items_main del-favorite" data-product="<?= $item->id ?>"></div>
                                            <?php else: ?>
                                                <div class="woman_like add-favorite" data-product="<?= $item->id ?>"></div>
                                            <?php  endif;
                                        else:
                                            $favorites = Favorites::find()->where(['product_id'=> $item->id, 'user_id' => Yii::$app->user->id])->one();
                                            if(!empty($favorites)){ ?>
                                                <div class="woman_like liked_items_main del-favorite" data-product="<?= $item->id ?>"></div>
                                            <?php } else{ ?>
                                                <div class="woman_like add-favorite" data-product="<?= $item->id ?>"></div>
                                            <?php    }
                                        endif;
                                        ?>
                                    </div>
                                </div>

                            </div>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <h3 style="margin: 10px"><?= Yii::t('app', 'Продукты еще не добавлены') ?></h3>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- paganation -->

<section class="paganation_all">
    <div class="container">
        <div class="paganation_div">
            <div class="pagination">
                <?php
                echo \yii\widgets\LinkPager::widget([
                    'pagination' => $pages,
                    'options' => ['class' => false],
                    'prevPageLabel' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                    'nextPageLabel' => '<i class="fa fa-angle-right" aria-hidden="true"></i>',
                    'activePageCssClass' => 'activeLi',

                ]); ?>
            </div>
        </div>
    </div>
</section>
