<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 12.06.2018
 * Time: 11:03
 */

use app\models\DeliveryTime;
use app\models\Rayon;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>


<!-- ordering -->
<section class="ordering">
    <div class="container">
        <div class="ordering_all">
            <div class="ordering_all-top_h1">
                <h1><?= Yii::t('app', 'ОФОРМЛЕНИЕ ЗАКАЗА') ?></h1>
            </div>

            <div class="panel-group" id="accordion">

            <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse">1. <?= Yii::t('app', 'Информация о покупателе') ?></a>
                        </h4>
                    </div>
                    <div id="collapse1" class="panel-collapse collapse in">

                        <div class="panel-body">
                            <?php if(Yii::$app->user->isGuest):?>
                            <div class="ordering_content">
                                <div class="lich-ad_all">
                                    <div class="lich-dan">
                                        <div class="lich-dan-h4">
                                            <h4><?= Yii::t('app', 'Личные данные') ?></h4>
                                        </div>
                                        <div class="lich-dan_inputs">
                                            <?php ActiveForm::begin(['id' => 'get-code-new', 'action' => Url::to(['/product/activate-new']), 'method' => 'post']); ?>
                                            <div class="lich-dan_inputs_1">

                                                <p>*<?= Yii::t('app', 'ф.и.о') ?></p>
                                                <input type="text" name="name" required="required">
                                            </div>
                                            <div class="lich-dan_inputs_1">
                                                <p>*<?= Yii::t('app', 'НОМЕР ТЕЛЕФОНА') ?></p>
                                                <input type="text" name="phone" required="required">
                                            </div>
                                            <div class="lich-dan_inputs_3btn">
                                                <button type="submit"><?= Yii::t('app', 'ПОЛУЧИТЬ КОД') ?></button>
                                                <?php ActiveForm::end(); ?>
                                                <p>*<?= Yii::t('app', 'ВВЕДИТЕ КОД') ?>: </p>
                                                <input type="text" name="code" id="code" disabled>
                                            </div>
                                            <a class="send-code-again-new"
                                               data-url="<?= Url::to(['site/send-again']) ?>"><?= Yii::t('app', 'Попробуйте снова') ?></a>

                                        </div>
                                    </div>
                                    <div class="ad-dost">
                                        <?php ActiveForm::begin(['id' => 'order-address', 'action' => Url::to(['/product/order-address']), 'method' => 'post']) ?>


                                        <input type="hidden" name="order_id" id="order_id">
                                        <div class="lich-dan-h4">
                                            <h4><?= Yii::t('app', 'Адрес доставки') ?></h4>
                                        </div>

                                        <div class="ad-dost-sec-p">
                                            <p>*<?= Yii::t('app', 'регион') ?></p>
                                            <div class="ad-dost-sec">

                                                <select name="region" class="region" required="required">
                                                    <?php if (!empty($region)): ?>
                                                        <option disabled
                                                                selected><?= Yii::t('app', 'Выберите Регион') ?></option>
                                                        <?php foreach ($region as $item): ?>

                                                            <option value="<?= $item->id ?>"><?= $item->region ?></option>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </select>
                                                <span>
	                                            	<i class="fa fa-sort-desc" aria-hidden="true"></i>
	                                            </span>
                                            </div>
                                        </div>
                                        <div class="ad-dost-sec-p">
                                            <p>*<?= Yii::t('app', 'РАЙОН') ?></p>
                                            <div class="ad-dost-sec">
                                                <select class="rayon" name="rayon" required="required">
                                                    <option disabled
                                                            selected><?= Yii::t('app', 'Выберите Район') ?></option>
                                                </select>
                                                <span>
	                                            	<i class="fa fa-sort-desc" aria-hidden="true"></i>
	                                            </span>
                                            </div>
                                        </div>
                                        <div class="uli-index-all">
                                            <div class="uli-index-top">
                                                <div class="uli-index-top-left">
                                                    <p>*<?= Yii::t('app', 'УЛИЦА') ?></p>
                                                    <input type="text" name="street">
                                                </div>
                                                <div class="uli-index-top-left">
                                                    <p><?= Yii::t('app', 'ИНДЕКС') ?></p>
                                                    <input type="text" name="index">
                                                </div>
                                            </div>
                                            <div class="uli-index-top">
                                                <div class="uli-index-top-left">
                                                    <p>*<?= Yii::t('app', 'НОМЕР ДОМА') ?></p>
                                                    <input type="text" name="number_dom">
                                                </div>
                                                <div class="uli-index-top-left">
                                                    <p><?= Yii::t('app', 'НОМЕР КВ') ?>.</p>
                                                    <input type="text" name="number_kv">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="ordering-map">
                                    <div class="ordering-map-h4">
                                        <h4><?= Yii::t('app', 'выберите место для доставки с карты') ?></h4>
                                    </div>
                                    <div class="ordering-map-img">

                                        <div id="floating-panel">
                                            <input onclick="deleteMarkers();" type=button value="Удалить выбраноое место">
                                        </div>
                                        <div class="field" id="map" style="height: 300px; width: 100%;">
                                            <script>
                                                var map;
                                                var markers = [];
                                                var i = 1;
                                                var lat;
                                                var lng;
                                                function initMap() {
                                                   // var lat1 = Number(document.getElementById("contacts-lat").value);
                                                   // var lng1 = Number(document.getElementById("contacts-lng").value);
                                                    var haightAshbury = {lat: 41.306688, lng: 69.281285};;



                                                    map = new google.maps.Map(document.getElementById('map'), {
                                                        zoom: 14,
                                                        center: haightAshbury,
                                                        mapTypeId: 'terrain'
                                                    });



                                                    // This event listener will call addMarker() when the map is clicked.
                                                    map.addListener('click', function(event) {
                                                        deleteMarkers();
                                                        if( i == 1){addMarker(event.latLng);  i=2;
                                                        }
                                                    });
                                                }


                                                // Adds a marker to the map and push to the array.
                                                function addMarker(location) {

                                                    var marker = new google.maps.Marker({
                                                        position: location,
                                                        map: map
                                                    });
                                                    markers.push(marker);

                                                    $('#lat').val(marker.getPosition().lat());
                                                    $('#lng').val(marker.getPosition().lng());

                                                }



                                                // Sets the map on all markers in the array.
                                                function setMapOnAll(map) {
                                                    for (var i = 0; i < markers.length; i++) {
                                                        markers[i].setMap(map);
                                                    }

                                                }

                                                // Removes the markers from the map, but keeps them in the array.
                                                function clearMarkers() {
                                                    setMapOnAll(null);
                                                }

                                                // Shows any markers currently in the array.
                                                function showMarkers() {
                                                    setMapOnAll(map);
                                                }

                                                // Deletes all markers in the array by removing references to them.
                                                function deleteMarkers() {
                                                    clearMarkers();
                                                    i = 1;
                                                    $('#lat').val('');
                                                    $('#lng').val('');
                                                    markers = [];
                                                }


                                            </script>
                                            <script async defer
                                                    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAlFroO7C1GYV5PKyg1IOXVvBp42eAZrBU&callback=initMap">
                                            </script>

                                        </div>
                                        <input type="text" name="lat" id="lat" class="hidden"/>
                                        <input type="text" name="lng" id="lng" class="hidden"/>
                                    </div>


                            </div>
                        </div>
                            <?php else: ?>
                          <?php ActiveForm::begin(['id' => 'order-address', 'action' => Url::to(['/product/order-address']), 'method' => 'post']) ?>

                                <div class="ordering_content">
                                    <div class="lich-ad_all">
                                        <div class="lich-dan">
                                            <div class="lich-dan-h4">
                                                <h4><?= Yii::t('app', 'Личные данные') ?></h4>
                                            </div>
                                            <div class="lich-dan_inputs">
                                                <div class="lich-dan_inputs_1">
                                                    <p>*<?= Yii::t('app', 'ф.и.о') ?></p>
                                                    <input type="text" name="name" required="required" value="<?= $user_info->name ?>">
                                                </div>
                                                <div class="lich-dan_inputs_1">
                                                    <p>*<?= Yii::t('app', 'НОМЕР ТЕЛЕФОНА') ?></p>
                                                    <input type="text" name="phone" required="required" value="<?= $user_info->phone ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ad-dost">
                                            <input type="hidden" name="order_id" id="order_id">
                                            <div class="lich-dan-h4">
                                                <h4><?= Yii::t('app', 'Адрес доставки') ?></h4>
                                            </div>

                                            <div class="ad-dost-sec-p">
                                                <p>*<?= Yii::t('app', 'регион') ?></p>
                                                <div class="ad-dost-sec">
                                                    <select name="region" class="region" required="required" value="1">
                                                        <?php if (!empty($region)): ?>
                                                            <option disabled
                                                                    selected><?= Yii::t('app', 'Выберите Регион') ?></option>
                                                            <?php foreach ($region as $item): ?>

                                                                <option value="<?= $item->id ?>" <?php
                                                                if($item->id == $user_info->rayon->region_id){
                                                                    echo 'selected';
                                                                }
                                                                ?>><?= $item->region ?></option>
                                                            <?php endforeach; ?>
                                                        <?php endif; ?>
                                                    </select>
                                                    <span>
	                                            	<i class="fa fa-sort-desc" aria-hidden="true"></i>
	                                            </span>
                                                </div>
                                            </div>
                                            <div class="ad-dost-sec-p">
                                                <p>*<?= Yii::t('app', 'РАЙОН') ?></p>
                                                <div class="ad-dost-sec">
                                                    <select class="rayon" name="rayon" required="required">
                                                        <option disabled
                                                                selected><?= Yii::t('app', 'Выберите Район') ?></option>
                                                        <?php $rayon = Rayon::find()->where(['region_id' => $user_info->rayon->region_id])->all(); ?>
                                                        <?php if(!empty($rayon)): ?>
                                                            <?php foreach ($rayon as $item): ?>

                                                                <option value="<?= $item->id ?>" <?php
                                                                if($item->id == $user_info->rayon->id){
                                                                    echo 'selected';
                                                                }
                                                                ?>><?= $item->rayon ?></option>
                                                            <?php endforeach; ?>
                                                        <?php endif; ?>
                                                    </select>
                                                    <span>
	                                            	<i class="fa fa-sort-desc" aria-hidden="true"></i>
	                                            </span>
                                                </div>
                                            </div>
                                            <div class="uli-index-all">
                                                <div class="uli-index-top">
                                                    <div class="uli-index-top-left">
                                                        <p>*<?= Yii::t('app', 'УЛИЦА') ?></p>
                                                        <input type="text" name="street" value="<?= $user_info->address ?>">
                                                    </div>
                                                    <div class="uli-index-top-left">
                                                        <p><?= Yii::t('app', 'ИНДЕКС') ?></p>
                                                        <input type="text" name="index" value="<?= $user_info->index ?>">
                                                    </div>
                                                </div>
                                                <div class="uli-index-top">
                                                    <div class="uli-index-top-left">
                                                        <p>*<?= Yii::t('app', 'НОМЕР ДОМА') ?></p>
                                                        <input type="text" name="number_dom" value="<?= $user_info->number_dom ?>">
                                                    </div>
                                                    <div class="uli-index-top-left">
                                                        <p><?= Yii::t('app', 'НОМЕР КВ') ?>.</p>
                                                        <input type="text" name="number_kv" value="<?= $user_info->number_kv ?>">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="ordering-map">
                                        <div class="ordering-map-h4">
                                            <h4><?= Yii::t('app', 'выберите место для доставки с карты') ?></h4>
                                        </div>
                                        <div class="ordering-map-img">

                                            <div id="floating-panel">
                                                <input onclick="deleteMarkers();" type=button value="Удалить выбраноое место">
                                            </div>
                                            <div class="field" id="map" style="height: 300px; width: 100%;">
                                                <script>
                                                    var map;
                                                    var markers = [];
                                                    var i = 1;
                                                    var lat;
                                                    var lng;
                                                    function initMap() {
                                                        // var lat1 = Number(document.getElementById("contacts-lat").value);
                                                        // var lng1 = Number(document.getElementById("contacts-lng").value);
                                                        var haightAshbury = {lat: 41.306688, lng: 69.281285};;



                                                        map = new google.maps.Map(document.getElementById('map'), {
                                                            zoom: 14,
                                                            center: haightAshbury,
                                                            mapTypeId: 'terrain'
                                                        });



                                                        // This event listener will call addMarker() when the map is clicked.
                                                        map.addListener('click', function(event) {
                                                            deleteMarkers();
                                                            if( i == 1){addMarker(event.latLng);  i=2;
                                                            }
                                                        });
                                                    }


                                                    // Adds a marker to the map and push to the array.
                                                    function addMarker(location) {

                                                        var marker = new google.maps.Marker({
                                                            position: location,
                                                            map: map
                                                        });
                                                        markers.push(marker);

                                                        $('#lat').val(marker.getPosition().lat());
                                                        $('#lng').val(marker.getPosition().lng());

                                                    }



                                                    // Sets the map on all markers in the array.
                                                    function setMapOnAll(map) {
                                                        for (var i = 0; i < markers.length; i++) {
                                                            markers[i].setMap(map);
                                                        }

                                                    }

                                                    // Removes the markers from the map, but keeps them in the array.
                                                    function clearMarkers() {
                                                        setMapOnAll(null);
                                                    }

                                                    // Shows any markers currently in the array.
                                                    function showMarkers() {
                                                        setMapOnAll(map);
                                                    }

                                                    // Deletes all markers in the array by removing references to them.
                                                    function deleteMarkers() {
                                                        clearMarkers();
                                                        i = 1;
                                                        $('#lat').val('');
                                                        $('#lng').val('');
                                                        markers = [];
                                                    }


                                                </script>
                                                <script async defer
                                                        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAlFroO7C1GYV5PKyg1IOXVvBp42eAZrBU&callback=initMap">
                                                </script>

                                            </div>
                                            <input type="text" name="lat" id="lat" class="hidden"/>
                                            <input type="text" name="lng" id="lng" class="hidden"/>
                                        </div>


                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="ordering_next1-div">
                                <button class="ordering_next1" type="submit" <?= Yii::$app->user->isGuest ? 'disabled' : '' ?>><?= Yii::t('app', 'ПРОДОЛЖИТЬ') ?></button>
                            </div>
                   <?php ActiveForm::end() ?>
                    </div>

                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse">2. <?= Yii::t('app','Время доставки') ?></a>
                    </h4>
                </div>
                <div id="collapse2" class="panel-collapse collapse">
                    <?php ActiveForm::begin(['id' => 'order-time', 'action' => Url::to(['/product/order-time']),'method' => 'post']) ?>
                    <input type="hidden" name="order_id" id="order_id_2">
                    <div class="panel-body">
                        <div class="ordering_content2">
                            <h1><?= Yii::t('app','Бесплатная доставка') ?></h1>
                            <div class="ordering_content22">
                                <img src="/img/car.png">
                                <p><?= Yii::t('app','Укажите время доставки удобное для Вас') ?></p>
                                <div class="ordering_content2-select">
                                    <select name="time">
                                        <?php $time = DeliveryTime::find()->all();?>
                                        <?php foreach ($time as $item): ?>
                                        <option value="<?= $item->id?>"><?= $item->time ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <span>
                                    	<i class="fa fa-sort-desc" aria-hidden="true"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ordering_prev2-divw">
                        <button class="ordering_prev2"><?= Yii::t('app','ВЕРНУТЬСЯ') ?></button>
                        <button class="ordering_next2" type="submit"><?= Yii::t('app','ПРОДОЛЖИТЬ') ?></button>
                    </div>
                    <?php ActiveForm::end() ?>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse">3. <?= Yii::t('app','Способ оплаты') ?></a>
                    </h4>
                </div>
                <div id="collapse3" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="ordering_content3">
                           <?php ActiveForm::begin(['id' => 'order-payment', 'action' => Url::to(['/product/order-payment']), 'method'=>'post']) ?>
                            <input type="hidden" name="order_id" id="order_id_3">
                                <div class="ordering_content3_4div">
                                    <?php $payment = \app\models\PaymentMethod::find()->where(['status' => 1])->all(); ?>
                                    <?php foreach ($payment as $k => $item ): ?>
                                    <div class="ordering_content3_img1">
                                        <input type="radio" id="ltest<?= $k ?>" name="radio-group" value="<?= $item->id ?>" <? if($k == 0){echo 'checked';} ?> />
                                        <label for="ltest<?= $k ?>"><span><img src="/uploads/<?= $item->path ?>"></span></label>
                                    </div>
                                    <?php endforeach; ?>

                                </div>


                        </div>
                    </div>
                    <div class="ordering_prev2-divw">
                        <button class="ordering_prev3"><?= Yii::t('app','ВЕРНУТЬСЯ') ?></button>
                        <button class="ordering_next2" type="submit" formaction="order-payment"><?= Yii::t('app','ПРОДОЛЖИТЬ') ?></button>
                    </div>
                    <?php ActiveForm::end() ?>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse">4. <?= Yii::t('app','Подтверждение заказа') ?></a>
                    </h4>
                </div>
                <div id="collapse4" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="ordering_content4">
                        </div>
                        <div class="ordering_content4-button-last">
                            <a class="ordering_prev" href="<?= Url::to(['/product/order-send']) ?>"><?= Yii::t('app','ПОДТВЕРДИТЬ ЗАКАЗ') ?></a>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        </div>
    </div>
</section>