<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 12.06.2018
 * Time: 17:55
 */
?>

<!-- welcome  -->
<section class="welcome thank_you">
    <div class="container">
        <div class="welcome_2divs">
            <div class="welcome_2divs_1">
                <h1><?= Yii::t('app','Спасибо за ваш заказ!') ?></h1>
            </div>
            <div class="welcome_2divs_2">
                <h2><?= Yii::t('app','ВАШ ЗАКАЗ ПРИНЯТ!') ?></h2>
                <p><?= Yii::t('app','Наши сотрудники уточнят наличие всех товаров в корзине и после проверки оператор свяжется с вами. Спасибо за покупку! ') ?></p>
                <p><?= Yii::t('app','Номер вашего заказа') ?>: <span>000<?=$id?>.</span> </p>

            </div>
        </div>
    </div>
</section>