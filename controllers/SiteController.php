<?php

namespace app\controllers;

use app\models\Application;
use app\models\Brands;
use app\models\Carousel;
use app\models\Country;
use app\models\forms\Login;
use app\models\Info;
use app\models\Orders;
use app\models\Pages;
use app\models\Partners;
use app\models\PaymentMethod;
use app\models\PriceFilter;
use app\models\Products;
use app\models\Rayon;
use app\models\Services;
use app\models\Size;
use app\models\Slider;
use app\models\Sms;
use app\models\Socials;
use app\models\User;
use app\models\UserInfo;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\forms\ContactForm;

class SiteController extends Controller
{
    public function init()
    {

        if (!empty(Yii::$app->request->cookies['language'])) {
            Yii::$app->language = Yii::$app->request->cookies['language'];
        } else {
            Yii::$app->language = 'ru';
        }
        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $info = Info::findOne(['id' => 1]);
        $carousel = Carousel::find()->orderBy(['order' => SORT_ASC])->all();
        $slider = Slider::find()
            ->limit(4)
            ->all();
        $slider2 = Slider::find()
            ->offset(4)
            ->limit(4)
            ->all();
        $forman = Products::find()
            ->where(['category_id' => 1])
            ->limit(8)
            ->orderBy(['id' => SORT_DESC])
            ->all();
        $forwoman = Products::find()
            ->where(['category_id' => 2])
            ->limit(8)
            ->orderBy(['id' => SORT_DESC])
            ->all();
        $forchild = Products::find()
            ->where(['category_id' => 3])
            ->limit(8)
            ->orderBy(['id' => SORT_DESC])
            ->all();
        $partners = Partners::find()->where(['status' => 1])->all();
        $payment = PaymentMethod::find()->where(['status' => 1])->all();
        $socials = Socials::find()->where(['status' => 1])->all();
        $model = new Application();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('app', true);
            $model = new Application();
            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->render('index', compact([
            'info',
            'carousel',
            'slider',
            'slider2',
            'forman',
            'forwoman',
            'forchild',
            'partners',
            'payment',
            'services',
            'socials',
        ]));
    }

    public function actionSetlanguage($lang)
    {
        $l = $lang;
        $langs = ['uz', 'ru'];
        if (in_array($l, $langs)) {
            \Yii::$app->language = $l;
            Yii::$app->session->set('app_lang', $l);
            $cookies = Yii::$app->response->cookies;
            $cookies->add(new \yii\web\Cookie([
                'name' => 'language',
                'value' => $l,
            ]));
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Login
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->getUser()->isGuest) {
            return $this->goHome();
        }

        $model = new Login();
        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->getUser()->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        $about = Pages::find()->where(['id' => 1])->one();
        $slider = Slider::find()
            ->limit(4)
            ->all();
        $slider2 = Slider::find()
            ->offset(4)
            ->limit(4)
            ->all();
        $partners = Partners::find()->where(['status' => 1])->all();
        $payment = PaymentMethod::find()->where(['status' => 1])->all();

        return $this->render('about', compact([
            'about',
            'slider',
            'slider2',
            'partners',
            'payment',
        ]));
    }

    public function actionPages($id)
    {
        $about = Pages::find()->where(['id' => $id])->one();
        $slider = Slider::find()
            ->limit(4)
            ->all();
        $slider2 = Slider::find()
            ->offset(4)
            ->limit(4)
            ->all();
        $partners = Partners::find()->where(['status' => 1])->all();
        $payment = PaymentMethod::find()->where(['status' => 1])->all();

        return $this->render('about', compact([
            'about',
            'slider',
            'slider2',
            'partners',
            'payment',
        ]));
    }
//    ================================================================================

    public function actionRestorePassword()
    {

        return $this->render('restore-password');
    }

    public function actionSignUp()
    {
        $username = Yii::$app->request->post('username');
        $password = Yii::$app->request->post('password');
        $email = Yii::$app->request->post('email');
        $phone = Yii::$app->request->post('phone');
        if (Yii::$app->request->post()) {
            $user = new User();
            $user->username = $username;
            $user->setPassword($password);
            $user->email = $email;
            $user->status = 0;
            if ($user->save()) {
                $user_info = new UserInfo();
                $user_info->user_id = $user->id;
                $user_info->phone = $phone;
                $user_info->save(false);
                $sms = new Sms();
                $sms->phone = $phone;
                $sms->sms = 1;
                $sms->save(false);
                return $this->render('activate', ['user_info' => $user_info]);
            }
        } else {
            return $this->render('sign-up');
        }
    }

    public function actionSendCode()
    {
        $phone = Yii::$app->request->post('phone');
        $code = Yii::$app->request->post('code');

        $user = UserInfo::find()->where(['phone' => $phone])->one();
        if (empty($code)) {
            if (!empty($user)) {
                return 1;
            } else {
                return 2;
            }
        } else {
            if (!empty($user)) {
                $id = $user->user_id;
                $sms = Sms::find()->where(['phone' => $phone, 'sms' => $code])->one();
                if (!empty($sms)) {
                    return $this->renderAjax('set-password', compact([
                        'id',
                    ]));
                } else {
                    return 3;
                }
            } else {
                return 2;
            }
        }

        return false;
    }

    public function actionActivate()
    {
        $phone = Yii::$app->request->post('phone');
        $code = Yii::$app->request->post('code');

        $user = UserInfo::find()->where(['phone' => $phone])->one();
        if (empty($code)) {
            if (!empty($user)) {
                return 1;
            } else {
                return 2;
            }
        } else {
            if (!empty($user)) {
                $id = $user->user_id;
                $sms = Sms::find()->where(['phone' => $phone, 'sms' => $code])->one();
                if (!empty($sms)) {
                    $user = User::findOne($id);
                    $user->status = 10;
                    $user->save(false);
                    return $this->renderAjax('welcome');

                } else {
                    return 3;
                }
            } else {
                return 2;
            }
        }

        return false;
    }

    public function actionSendAgain()
    {
        return 1;
    }

    public function actionSetPassword()
    {
        $id = Yii::$app->request->get('id');
        $password = Yii::$app->request->get('password');
        $user = User::findOne($id);
        $user->setPassword($password);
        if ($user->save()) {
            return $this->goHome();
        }
    }

    public function actionChangePassword($id)
    {
        $password = Yii::$app->request->get('password');
        if ($password) {
            $user = User::findOne($id);
            $user->setPassword($password);
            if ($user->save()) {
                return $this->goHome();
            }
        } else {
            return $this->render('change-password', ['id' => $id]);
        }
    }

    public function actionCabinet()
    {
        $id = Yii::$app->user->id;
        $user = User::findOne($id);
        $user_info = UserInfo::find()->where(['user_id' => $id])->one();
        $orders = Orders::find()->where(['user_id' => $id])->all();

        return $this->render('cabinet', compact([
            'user',
            'user_info',
            'orders',
        ]));

    }

    public function actionChangeUser()
    {

        $id = Yii::$app->user->id;
        $user = User::findIdentity($id);
        $user_info = UserInfo::find()->where(['user_id' => $id])->one();

        $phone = Yii::$app->request->get('phone');
        $username = Yii::$app->request->get('username');
        $name = Yii::$app->request->get('name');
        $emial = Yii::$app->request->get('email');
        if (Yii::$app->request->get()) {
            $user->username = $username;
            $user->email = $emial;
            $user_info->name = $name;
            $user_info->phone = $phone;
            if ($user_info->save(false) && $user->save(false)) {
                return $this->render('cabinet', compact([
                    'user',
                    'user_info',
                ]));
            } else {
                return 'error';
            }
        }


    }

    public function actionRegion($region_id)
    {
        //Yii::$app->response->format = yii\web\Response::FORMAT_JSON;
        $this->layout = false;
        $rayon = Rayon::find()->where(['region_id' => $region_id])->all();
        $return = '';
        if (!empty($rayon)) {
            foreach ($rayon as $item) {
                $return .= '<option value="' . $item->id . '" >' . $item->rayon . '</option>';
            }
        } else {
            $return .= '<option> - </option>';
        }

        return $return;
    }

    public function actionChangeAddress(){
        $rayon = Yii::$app->request->get('rayon');
        $index = Yii::$app->request->get('index');
        $street = Yii::$app->request->get('street');
        $n_dom = Yii::$app->request->get('number_dom');
        $n_kv = Yii::$app->request->get('number_kv');

        $user_id = Yii::$app->user->id;
        $user = User::findOne($user_id);
        $user_info = UserInfo::find()->where(['user_id'=> $user_id])->one();
        $user_info->region_id = $rayon;
        $user_info->index = $index;
        $user_info->address = $street;
        $user_info->number_dom = $n_dom;
        $user_info->number_kv = $n_kv;
        if($user_info->save()){
            return $this->render('cabinet', compact([
                'user',
                'user_info',
            ]));
        }
    }

//    ================================================================================


}
