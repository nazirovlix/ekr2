<?php
/**
 * Created by PhpStorm.
 * User: SoS
 * Date: 05.06.2018
 * Time: 13:48
 */

namespace app\controllers;


use app\models\Application;
use app\models\Brands;
use app\models\Carousel;
use app\models\Category;
use app\models\Country;
use app\models\Favorites;
use app\models\forms\Login;
use app\models\Info;
use app\models\OrderProducts;
use app\models\Orders;
use app\models\Pages;
use app\models\Partners;
use app\models\PaymentMethod;
use app\models\PriceFilter;
use app\models\ProductImages;
use app\models\Products;
use app\models\Region;
use app\models\Services;
use app\models\Size;
use app\models\Slider;
use app\models\Sms;
use app\models\Socials;
use app\models\User;
use app\models\UserInfo;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\forms\ContactForm;


class ProductController extends Controller
{
    public function init()
    {

        if (!empty(Yii::$app->request->cookies['language'])) {
            Yii::$app->language = Yii::$app->request->cookies['language'];
        } else {
            Yii::$app->language = 'ru';
        }
        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {

    }

    // Category

    public function actionCategory($id)
    {

        $category = Category::find()->where(['id' => $id])->one();

        $query = Products::find()->filterWhere(['category_id' => $id]);
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 15, 'forcePageParam' => false, 'pageSizeParam' => false]);
        $products = $query->offset($pages->offset)->limit($pages->limit)->all();

        $prices = PriceFilter::find()->where(['status' => 1])->all();
        $sizes = Size::find()->where(['status' => 1])->orderBy(['size' => SORT_ASC])->all();
        $country = Country::find()->where(['status' => 1])->all();
        $brands = Brands::find()->where(['status' => 1])->all();

        return $this->render('category', compact([
            'pages',
            'products',
            'sizes',
            'country',
            'brands',
            'prices',
            'category',
        ]));

    }

    public function actionCategoryFilter()
    {

        Yii::$app->response->format = yii\web\Response::FORMAT_JSON;
        $id = Yii::$app->request->get('id');

        $prices = Yii::$app->request->get('price');
        $size = Yii::$app->request->get('size');
        $country = Yii::$app->request->get('country');
        $brand = Yii::$app->request->get('brand');


        $query = Products::find()->distinct()
            ->filterWhere(['category_id' => $id])
            ->joinWith('sizess')
            ->andFilterWhere(['in', 'size_id', $size])
            ->andFilterWhere(['in', 'products.country_id', $country])
            ->andFilterWhere(['in', 'products.brand_id', $brand]);

        if (!empty($prices)):
            $price = PriceFilter::find()->where(['id' => $prices])->one();
            if ($price->to == null) {
                $query = $query->andFilterWhere(['or',
                    ['>', 'products.price', $price->from],
                    ['>', 'products.discount_price', $price->from],
                ]);
            } else {
                $query = $query->andFilterWhere(['or',
                    ['between', 'products.price', $price->from, $price->to],
                    ['between', 'products.discount_price', $price->from, $price->to],
                ]);
            }
        endif;

        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 15, 'forcePageParam' => false, 'pageSizeParam' => false]);
        $products = $query->offset($pages->offset)->limit($pages->limit)->all();

        return [
            'content' => $this->renderAjax('forman-ajax', compact([
                'products',
            ])),
            'url' => Url::current(['product/category']),
        ];
    }

    public function actionProduct($id)
    {
        $product = Products::find()
            ->where(['id' => $id, 'status' => 1])
            ->one();

        $most = Products::find()
            ->where(['status' => 1])
            ->orderBy(['id' => SORT_DESC])
            ->limit(4)
            ->all();


        return $this->render('product', compact([
            'product',
            'most',
        ]));

    }

    public function actionAddFavorite()
    {

        $product_id = Yii::$app->request->get('product_id');

        if (Yii::$app->user->isGuest) {
            $session = Yii::$app->session;
            $session->open();
            $cart = new Favorites();
            $cart->addFavorites($product_id);
            $f = $_SESSION['favorites'];
            return count($f);
        } else {
            $user = Yii::$app->user->id;
            $favotire = new Favorites();
            $favotire->product_id = $product_id;
            $favotire->user_id = $user;
            if ($favotire->save()) {
                $f = Favorites::find()->where(['user_id'=> $user])->count();
                return $f;
            }
        }
        return false;

    }

    public function actionDelFavorite()
    {

        $product_id = Yii::$app->request->get('product_id');

        if (Yii::$app->user->isGuest) {
            $session = Yii::$app->session;
            $session->open();
            $cart = new Favorites();
            $cart->delFavorites($product_id);
            $f = $_SESSION['favorites'];
            return count($f);
        } else {
            $user = Yii::$app->user->id;
            Favorites::deleteAll(['user_id' => $user, 'product_id' => $product_id]);
            $f = Favorites::find()->where(['user_id'=> $user])->count();
            return $f;
        }
        return false;

    }

    public function actionFavorites()
    {
        $session = Yii::$app->session['favorites'];
        if (!empty($session)):
            if (Yii::$app->user->isGuest) {
                $favs = $_SESSION['favorites'];
                $count = count($favs);
                $query = Products::find()->where(['in', 'id', $favs]);
                $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 8, 'forcePageParam' => false, 'pageSizeParam' => false]);
                $favorites = $query->offset($pages->offset)->limit($pages->limit)->all();
            } else {
                $favs = ArrayHelper::getColumn(Favorites::find()->select('product_id')
                    ->where(['user_id' => Yii::$app->user->id])->all(), 'product_id');

                $query = Products::find()->where(['in', 'id', $favs]);
                $count = $query->count();
                $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 8, 'forcePageParam' => false, 'pageSizeParam' => false]);
                $favorites = $query->offset($pages->offset)->limit($pages->limit)->all();
            }
        endif;


        return $this->render('favorites', compact([
            'favorites',
            'count',
            'pages'
        ]));

    }

    public function actionAddToCart(){


        $session = Yii::$app->session;
        $session->open();

        $id  = Yii::$app->request->get('id');
        $color = Yii::$app->request->get('color');
        $size = Yii::$app->request->get('size');
        $product = Products::findOne($id);

        if (empty($product)) return false; // елси пусто
        $cart = new Products();
        if($cart->addToCart($product, $color, $size)){
            Yii::$app->session->setFlash('add');
            return $this->redirect(Yii::$app->request->referrer);
        }

    }

    public function actionBasket(){
        $session = Yii::$app->session['cart'];

        if(!empty($session )){
            $session = $_SESSION['cart'];
        }
        $most = Products::find()
            ->where(['status' => 1])
            ->orderBy(['id' => SORT_DESC])
            ->limit(4)
            ->all();

        return $this->render('basket',compact([
            'session',
            'most',
        ]));
    }

    public function actionRemoveCart(){
        unset($_SESSION['cart.sum']);
        unset($_SESSION['cart.qty']);
        unset($_SESSION['cart']);
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionPlusCart($id){
        $qty = 1;
        $price = $_SESSION['cart'][$id]['price'];
        $_SESSION['cart'][$id]['qty'] += 1;
        $_SESSION['cart.qty'] = isset($_SESSION['cart.qty']) ? $_SESSION['cart.qty'] + $qty : $qty;
        $_SESSION['cart.sum'] = isset($_SESSION['cart.sum']) ? $_SESSION['cart.sum'] + $qty* $price : $qty *  $price;
        return true;
    }

    public function actionMinusCart($id){

        $qty = 1;
        $price = $_SESSION['cart'][$id]['price'];
        if($_SESSION['cart'][$id]['qty'] != 0){
            $_SESSION['cart'][$id]['qty'] -= $qty;  // если товар уже есть, добавляет -1 к qty
        }
        $_SESSION['cart.qty'] = isset($_SESSION['cart.qty']) ? $_SESSION['cart.qty'] - $qty : $qty;
        $_SESSION['cart.sum'] = isset($_SESSION['cart.sum']) ? $_SESSION['cart.sum'] - $qty*$price : $qty * $price;
        return true;
    }

    public function actionChange($id){

        $id_product = $_SESSION['cart'][$id]['id'];
        $cart = new Products();
        $cart->recalc($id);

        $product = Products::find()
            ->where(['id' => $id_product, 'status' => 1])
            ->one();
        $most = Products::find()
            ->where(['status' => 1])
            ->orderBy(['id' => SORT_DESC])
            ->limit(4)
            ->all();
        return $this->render('product', compact([
            'product',
            'most',
            'id_product'
        ]));

    }

    public function actionDelCart($id){

        $cart = new Products();
        $cart->recalc($id);
        return true;

    }

    public function actionOrder(){

        $session = $_SESSION['cart'];

        if(!Yii::$app->user->isGuest){
            $id = Yii::$app->user->id;
            $user = User::findOne($id);
            $user_info = UserInfo::findOne(['user_id'=>$id]);
        }
        $region = Region::find()->all();
        return $this->render('order',compact([
            'session',
            'user_info',
            'user',
            'region',
        ]));
    }

    public function actionActivateNew()
    {
        $name = Yii::$app->request->post('name');
        $phone = Yii::$app->request->post('phone');

        $order = Orders::find()->where(['phone' => $phone])->one();
        if(empty($order)){
            $order = new Orders();
        }
        $order->name = $name;
        $order->phone = $phone;
        $sms = new Sms();
        $sms->phone = $phone;
        $sms->sms = 1;
        if($order->save(false) && $sms->save(false)){
            return $order->id;
        }
        return false;

    }

    public function actionCheckCodePhone(){
        $this->layout = false;
        $phone = Yii::$app->request->get('phone');
        $code = Yii::$app->request->get('code');

        if($phone){
            $sms = Sms::find()->where(['phone' => $phone])->one();
            if($sms->sms == $code){
                return 1;
            } else {
                return 2;
            }
        } else{
            return false;
        }


    }

    public function actionOrderAddress(){

        $order_id = Yii::$app->request->post('order_id');

        $name = Yii::$app->request->post('name');
        $phone = Yii::$app->request->post('phone');

        $street = Yii::$app->request->post('street');
        $rayon = Yii::$app->request->post('rayon');
        $index = Yii::$app->request->post('index');
        $number_dom = Yii::$app->request->post('number_dom');
        $number_kv = Yii::$app->request->post('number_kv');
        $lat = Yii::$app->request->post('lat');
        $lng = Yii::$app->request->post('lng');

        if(empty($order_id)):
            $order = new Orders();
        else:
            $order = Orders::findOne($order_id);
        endif;
        if(!Yii::$app->user->isGuest){
            $order->name = $name;
            $order->phone = $phone;
        }
        $order->street = $street;
        $order->index = $index;
        $order->region_id = $rayon;
        $order->number_dom = $number_dom;
        $order->number_kv = $number_kv;
        $order->lat = $lat;
        $order->lang = $lng;

        if($order->save(false)){
            return $order->id;
        }
        else{
            return false;
        }

    }

    public function actionOrderTime() {

        $order_id = Yii::$app->request->post('order_id');
        $time = Yii::$app->request->post('time');

        $order = Orders::findOne($order_id);
        $order->delivery_time_id = $time;
        if($order->save(false)){
            return true;
        } else{
            return false;
        }
         return false;
    }

    public function actionOrderPayment() {

        $session = Yii::$app->session['cart'];

        if(empty($session)){
            $session = $_SESSION['cart'];
        }

        $order_id = Yii::$app->request->post('order_id');
        $time = Yii::$app->request->post('radio-group');
        $order = Orders::findOne($order_id);
        $_SESSION['cart.code'] = $order_id;

        $order = Orders::findOne($order_id);
        $order->payment_id = $time;

        if(!Yii::$app->user->isGuest){
            $order->user_id = Yii::$app->user->id;
        }
        $order->amount = $_SESSION['cart.sum'];
        $this->saveOrderItems($session, $order_id);

        if($order->save(false)){
            return $this->renderAjax('order-ajax', compact([
                'order',
                'session',
            ]));
        } else{
            return false;
        }
        return false;
    }

    protected function saveOrderItems ($items, $order_id) {         // Запись всех продуктов из Order
        foreach ($items as $id => $item) {
            $order_items = new OrderProducts();

            $order_items->orders_id = $order_id;
            $order_items->product_id = $item['id'];
            $order_items->quantity = $item['qty'];
            $order_items->discount = $item['price']*$item['qty'];
            $order_items->product_price = $item['price'];
            $order_items->color = $item['color'];
            $order_items->size = $item['size'];
            $order_items->save();
        }
    }

    public function actionOrderSend() {

        $id = $_SESSION['cart.code'];
        $order = Orders::findOne($id);
        $order->state = 0;

        $session = Yii::$app->session;
        $session->remove('cart');
        $session->remove('cart.sum');
        $session->remove('cart.qty');

        if($order->save(false)){
            return $this->render('thanks',['id' => $id]);
        }

    }

    public function actionSearch(){

        $q = Yii::$app->request->get('q');


        $query = Products::find();
        if(Yii::$app->language =='ru'){
            $query = $query->filterWhere(['like', 'title_ru', $q]);
        } else{
            $query = $query->filterWhere(['like', 'title_uz', $q]);
        }
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 16, 'forcePageParam' => false, 'pageSizeParam' => false]);
        $products = $query->offset($pages->offset)->limit($pages->limit)->all();

        return $this->render('search',compact([
            'q',
            'pages',
            'products',
        ]));

    }

}