<?php
/**
 * Created by PhpStorm.
 * User: Farhodjon
 * Date: 10.03.2018
 * Time: 15:17
 */

use app\modules\admin\widgets\Menu;
use yii\helpers\Url;

?>
<div class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <?php
            try {
                echo Menu::widget([
                    'options' => [ 'id' => 'sidebarnav' ],
                    'submenuTemplate' => "\n<ul aria-expanded='false' class='collapse'>\n{items}\n</ul>\n",
                    'badgeClass' => 'label label-rouded label-primary pull-right',
                    'activateParents' => true,
                    'items' => [
                        [
                            'label' => '',
                            'options' => [ 'class' => 'nav-devider' ]
                        ],
                        [
                            'label' => 'Home',
                            'options' => [ 'class' => 'nav-label' ]
                        ],
                        [
                            'label' => 'Dashboard',
                            'url' => ['default/index'],
                            'icon' => '<i class="fa fa-tachometer"></i>',
                        ],
                        [
                            'label' => 'App',
                            'options' => [ 'class' => 'nav-label' ]
                        ],
                        [
                            'label' => 'Home page',
                            'url' => '#',
                            'icon' => '<i class="fa fa-home"></i>',
                            'items' => [
                                [
                                    'label' => 'Info about "Ekrossovki"',
                                    'url' => Url::to(['info/view','id' => 1]),
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Carousel',
                                    'url' => ['carousel/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Slider',
                                    'url' => ['slider/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Partners',
                                    'url' => ['partners/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Payment methods',
                                    'url' => ['payment-method/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Services',
                                    'url' => ['services/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Social networks',
                                    'url' => ['socials/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],

                            ]
                        ],

                        [
                            'label' => 'Pages',
                            'url' => '#',
                            'icon' => '<i class="fa  fa-file-o"></i>',
                            'items' => [
                                [
                                    'label' => 'About',
                                    'url' => Url::to(['pages/view', 'id'=>1]),
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Payment',
                                    'url' => Url::to(['pages/view', 'id'=>2]),
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Delivery',
                                    'url' => Url::to(['pages/view', 'id'=>3]),
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Return',
                                    'url' => Url::to(['pages/view', 'id'=>4]),
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Offer',
                                    'url' => Url::to(['pages/view', 'id'=>5]),
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],



                            ]
                        ],
                        [
                            'label' => 'Products',
                            'url' => '#',
                            'icon' => '<i class="fa fa-database"></i>',
                            'items' => [
                                [
                                    'label' => 'Category',
                                    'url' => Url::to(['category/index']),
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Products',
                                    'url' => Url::to(['products/index']),
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Filters',
                                    'options' => [ 'class' => 'nav-label' ]
                                ],
                                [
                                    'label' => 'Price Filter',
                                    'url' => ['price-filter/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Brands',
                                    'url' => ['brands/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Countries',
                                    'url' => ['country/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Product Sizes',
                                    'url' => ['size/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Product Colors',
                                    'url' => ['product-color/index'],
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],


                            ]
                        ],
                        [
                            'label' => 'Orders',
                            'url' => '#',
                            'icon' => '<i class="fa  fa-file-o"></i>',
                            'items' => [
                                [
                                    'label' => 'Orders',
                                    'url' => Url::to(['orders/index']),
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Region',
                                    'url' => Url::to(['region/index']),
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],

                                [
                                    'label' => 'Rayon',
                                    'url' => Url::to(['rayon/index']),
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],
                                [
                                    'label' => 'Delivery Time',
                                    'url' => Url::to(['delivery-time/index']),
                                    'icon' => '<i class="fa fa-angle-double-right"></i>',
                                ],




                            ]
                        ],
                    ]
                ]);
            } catch ( Exception $e ) {
            }
            
            ?>
        </nav>
    </div>
</div>
