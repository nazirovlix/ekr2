<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PriceFilter */

$this->title = Yii::t('app', 'Create Price Filter');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Price Filters'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="price-filter-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
