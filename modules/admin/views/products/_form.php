<?php

use app\models\Brands;
use app\models\Category;
use app\models\Country;
use app\models\ProductColor;
use app\models\Size;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use trntv\filekit\widget\Upload;
use unclead\multipleinput\MultipleInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Products */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'file')->widget(
                Upload::className(),
                [
                    'url' => ['/admin/file-storage/upload'],
                    'sortable' => true,
                    'maxFileSize' => 10 * 1024 * 1024, // 10 MiB

                ]);
            ?>
        </div>
        <div class="col-md-9">
            <?= $form->field($model, 'title_ru')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'title_uz')->textInput(['maxlength' => true]) ?>

            <div class="form-group field-products-code">
                <label class="control-label" for="products-code">Code</label>
                <div class="input-group">
                    <div class="input-group-addon" style="color:#333; padding-right:20px">#</div>
                    <input type="text" id="products-code" class="form-control" name="Products[code]" value="<?= $model->isNewRecord ? '' : $model->code ?>" aria-invalid="false">

                    <div class="help-block"></div>
                </div>
            </div>
        </div>

    </div>




    <?php echo $form->field($model, 'attachments')->widget(
        Upload::className(),
        [
            'url' => ['/admin/file-storage/upload'],
            'sortable' => true,

            'maxFileSize' => 10000000, // 10 MiB
            'maxNumberOfFiles' => 16
        ]);
    ?>
    <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(Category::find()->all(),'id','title_ru')) ?>

    <?= $form->field($model, 'price')->textInput() ?>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group field-products-discount">
                <label class="control-label" for="products-discount">Discount</label>
                <div class="input-group">
                    <div class="input-group-addon" style="color:#333; padding-right:20px">%</div>

                    <input type="text" id="products-discount" onkeyup="discount()" class="form-control" name="Products[discount]" value="<?= $model->isNewRecord ? '' : $model->discount ?>" aria-invalid="false">

                <div class="help-block"></div>
                </div>
            </div>
        </div>
        <div class="col-md-6">

            <?= $form->field($model, 'discount_price')->textInput() ?>

        </div>
    </div>



    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'sizes')->widget(MultipleInput::className(), [
                'columns' => [
                    [
                        'name' => 'size_id',
                        'type' => 'dropDownList',
                        'items' => ArrayHelper::map(Size::find()->where(['status' => 1])->all(), 'id', 'size'),
                        'options' => [
                            'prompt' => 'Choose size'
                        ]
                    ],

                ],
                'addButtonOptions' => ['class' => 'btn btn-success']
            ])->label('Sizes') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'colors')->widget(MultipleInput::className(), [
                'columns' => [
                    [
                        'name' => 'color_name',
                        'type' => 'dropDownList',
                        'items' => ArrayHelper::map(ProductColor::find()->all(), 'id', 'color_name_ru'),
                        'options' => [
                            'prompt' => 'Choose colors'
                        ]
                    ],

                ],
                'addButtonOptions' => ['class' => 'btn btn-success']
            ])->label('Product colors') ?>
        </div>

    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'country_id')->dropDownList(ArrayHelper::map(Country::find()->where(['status' => 1 ])->all(),'id','country_ru')) ?>
        </div>
        <div class="col-md-6">

            <?= $form->field($model, 'brand_id')->dropDownList(ArrayHelper::map(Brands::find()->where(['status' => 1 ])->all(),'id','name')) ?>
        </div>
    </div>
    <?= $form->field($model, 'content_ru')->widget(CKEditor::className(), [
    'editorOptions' => ElFinder::ckeditorOptions('elfinder', [ 'height' => 300 ]),
    ]); ?>

    <?= $form->field($model, 'content_uz')->widget(CKEditor::className(), [
    'editorOptions' => ElFinder::ckeditorOptions('elfinder', [ 'height' => 300 ]),
    ]); ?>






    <?= $form->field($model, 'status')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
