$('.dropify').dropify({
    messages: {
        'default': 'Drag and drop a file here or click',
        'replace': 'Drag and drop or click to replace',
        'remove':  'Remove',
        'error':   'Ooops, something wrong happended.'
    }
});

var tag = $('#admin-content p:first:has("a")');
$('.page-titles .col-md-5.align-self-center').html(tag.html());
tag.remove();

function discount() {
    var val_p = $('#products-price').val();
    var val_d = $('#products-discount').val();
    var val_dp = $('#products-discount_price');
    var price = 0;
    price = val_p - (val_p*val_d)/100;

    val_dp.attr('value',price);


}

