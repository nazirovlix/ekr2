$('.myslick').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 3
});
// slider top single
$('.single-item_ek').slick({
	  autoplay: true,
    autoplaySpeed: 10000,
    prevArrow: $(".slick-prev"),
    nextArrow:$(".slick-next")
});
// slider 4 divs
$('.single-item_ek4').slick({
	  autoplay: true,
    autoplaySpeed: 10000,
    prevArrow: $(".slick-prev4"),
    nextArrow:$(".slick-next4"),

  slidesToShow: 2,
  slidesToScroll: 2,

      responsive: [
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        // dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    
  ]
});
// for liked items
    $(".woman_like").click(function(){
        $(this).toggleClass("liked_items_main");
    });
        $(".woman_card").click(function(){
        $(this).toggleClass("carded_items_main");
    });
// slider brend
$('.single-item').slick({
	  autoplay: true,
    autoplaySpeed: 10000,

    prevArrow: $(".slick-prev"),
    nextArrow:$(".slick-next"),


});



//brend slider
$('.brend_alldiv').slick({
  slidesToShow: 6,
  slidesToScroll: 6,
  autoplay: true,
  autoplaySpeed: 10000,
  // dots: true,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 4,
        infinite: true,
        // dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    
  ]
});
// slider for men breakpoint
$('.slick_break').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav2',

    prevArrow: $(".slick-preva"),
    nextrrow:$(".slick-nexta")

});

$('.slider-nav2').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  asNavFor: '.slick_break',
  focusOnSelect: true,



    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    }
    
  ]

});







// acardion
  var acc = document.getElementsByClassName("accordionv1");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("activev1");
    var panelv1 = this.nextElementSibling;
    if (panelv1.style.maxHeight){
      panelv1.style.maxHeight = null;
    } else {
      panelv1.style.maxHeight = panelv1.scrollHeight + "px";
    } 
  });
}


         $("#zoom_03").ezPlus({
                 gallery: 'gallery_01',
                 cursor: 'pointer',
                 galleryActiveClass: "active",
                 imageCrossfade: true,
                 loadingIcon: "/images/spinner.gif"
         });

         $("#zoom_03").bind("click", function (e) {
                 var ez = $('#zoom_03').data('ezPlus');
                 ez.closeAll(); //NEW: This function force hides the lens, tint and window
                 return false;
         });



$('#gallery_01').slick({
  slidesToShow: 4,
  slidesToScroll: 4,
  // autoplay: true,
  // autoplaySpeed: 10000,
  // dots: true,

  prevArrow: $(".slick-preva"),
  nextArrow:$(".slick-nexta"),  
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        // dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    }
    
  ]
});





    // $('#myModal').load('show',  function(){
    // });

    $('.cons_top_slider1').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: '.all4_slider_bot',
          prevArrow: $(".slick-prevp"),
        nextArrow:$(".slick-nextp")  
   
      });
      $('.all4_slider_bot').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.cons_top_slider1',
        prevArrow: $(".slick-prevp"),
        nextArrow:$(".slick-nextp"),
         focusOnSelect: true,


          responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        // dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    
  ]

      }); 

$('#myModal').on('shown.bs.modal', function (e) {
  $('.cons_top_slider1').slick('setPosition');
    $('.all4_slider_bot').slick('setPosition');
})

// for prodoct 
$("body").on("click", ".for_woman-1div_disap", function(e){
  e.preventDefault();
  $(this).parent().parent().remove();
});


   // for plus and minus 
  $(".prod_rew_amt_ctrl_btn").click(function () {

        var amountInput = $(this).siblings(".prod_rew_amt_ctrl_inp");
        var amount = parseInt(amountInput.val());
        if ($(this).hasClass("minus"))
        {
          if (amount > 1)
          {
            amountInput.val(--amount);
          }
        }
        else if ($(this).hasClass("plus"))
        {
          amountInput.val(++amount);
        }
      });

$(".prod_rew_amt_ctrl_inp").keyup(function (){

        var checkValue = parseInt($(this).val());

        if (isNaN(checkValue) || checkValue == 0)
        {
          checkValue = 1;
          $(this).val(checkValue);
        }
        else
        {
          $(this).val(checkValue);
        }
      });
// 
$(document).ready(function(){
    $(".header3-div-register.mypersonal").click(function(){
        $(".header3-div-register.mypersonal ul").toggleClass("main_ul-none_bl");
    });
});



//info div hide and show
// $(document).ready(function(){
//     $(".info-_btn-red, .info-tab_all-1-frs_btn button").click(function(){
//         $(".info-tab_all-1-frs").toggleClass("div-m-active");
//         $(".info-tab_all-1-second_all").toggleClass("div-m-active");
//     });
// });

$(document).on('click','.info-tab_all-1-second_all button', function () {
   $('.info-tab_all-1-second_all').removeClass('div-m-active');
   $('.info-tab_all-1-frs').addClass('div-m-active');
});
$(document).on('click','.info-_btn-red2', function () {
    $('.info_all_second-all_wbtn').removeClass('div-m-active-2');
    $('.info_all_second-all_wbtn2').addClass('div-m-active-2');
});

// $(document).ready(function(){
//     $(".info-_btn-red2").click(function(){
//         $(".info_all_second-all_wbtn").toggleClass("div-m-active-2");
//         $(".info_all_second-all_wbtn2").toggleClass("div-m-active-2");
//     });
// });

// collaps add class next and prev
$(".prev").click(function(e){
  e.preventDefault();
  $(this).parent().parent().parent().prev().find(".panel-heading .panel-title a").click();
});
//
// $(".next").click(function(e){
//   e.preventDefault();
//   $(this).parent().parent().parent().next().find(".panel-heading .panel-title a").click();
// })




// acardion for referance

var acc = document.getElementsByClassName("accordion");
var i;

// for (i = 0; i < acc.length; i++) {
//   acc[i].addEventListener("click", function() {
//     this.classList.toggle("active");
//     var panel = this.nextElementSibling;
//     if (panel.style.maxHeight){
//       panel.style.maxHeight = null;
//     } else {
//       panel.style.maxHeight = panel.scrollHeight + "px";
//     }
//   });
// }


