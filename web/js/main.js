///  Main JS ///////////////////

$("#language").change(function () {
    var val = $(this).val();
    var url = $(this).data('url');

    if (val == 'saab') {
        document.location.href = url + '?lang=uz';
    } else {
        document.location.href = url + '?lang=ru';
    }


});

//LOADing
function runLoader() {
    $('.loader').removeClass('hidden');
}

function stopLoader() {
    $('.loader').addClass('hidden');
}

$(document).on('change', '#filter-category input', function () {
    var form = $('#filter-category');
    form.submit();

});

// Filter for Category
$('#filter-category').submit(function (e) {
    e.preventDefault();
    var form = $(this);
    $.ajax({
        url: form.attr('action'), //Change
        type: form.attr('method'),
        data: form.serialize(),
        beforeSend: function () {
            runLoader();
        },
        success: function (res) {
            $('.products_filter').html(res.content);
            window.history.pushState({}, "", res.url);
        },
        complete: function () {
            stopLoader();
        }

    });
});

// drop down menu  mobile
$(".drop_ol-my a").click(function () {
    $(".drop_ol-my_block").toggleClass("drop_ol-my_block-mobil");
});

// Add to Favorites
$(document).on('click', '.add-favorite', function (e) {
    e.preventDefault();
    var like = $(this);
    var prod_id = like.data('product');
    like.removeClass('add-favorite');
    like.addClass('del-favorite');
    $.ajax({
        url: '/product/add-favorite',
        data: {product_id: prod_id},
        type: 'GET',
        success: function (res) {
            $('.favorites-count').html(res);
            //alert('Add');
        },
        error: function () {
            alert('Ошибка при добавлении в избренные');
        }
    })
});
$(document).on('click', '.add-tofavorites', function (e) {
    e.preventDefault();
    var like = $(this);
    var prod_id = like.data('product');
    like.removeClass('add-tofavorites');
    $.ajax({
        url: '/product/add-favorite',
        data: {product_id: prod_id},
        type: 'GET',
        success: function (res) {
            $('.favorites-count').html(res);
            alert('Продукт добавлен в избранные');
        },
        error: function () {
            alert('Ошибка при добавлении в избренные');
        }
    })
});

// Delete for Favorites
$(document).on('click', '.del-favorite', function (e) {
    e.preventDefault();
    var like = $(this);
    var prod_id = like.data('product');
    like.removeClass('del-favorite');
    like.addClass('add-favorite');
    $.ajax({
        url: '/product/del-favorite',
        data: {product_id: prod_id},
        type: 'GET',
        success: function (res) {
            $('.favorites-count').html(res);
        },
        error: function () {
            alert('Ошибка при удаление из избренныех');
        }
    })
});
$(document).on('click', '.plus', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    $.ajax({
        url: '/product/plus-cart',
        data: {id: id},
        type: 'GET',
        beforeSend: function () {
            runLoader();
        },
        success: function () {
            location.reload();
        },
        error: function () {
            alert('Ошибка');
        },
        complete: function () {
            stopLoader();
        }
    })
});

$(document).on('click', '.minus', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    $.ajax({
        url: '/product/minus-cart',
        data: {id: id},
        type: 'GET',
        beforeSend: function () {
            runLoader();
        },
        success: function () {
            location.reload();
        },
        error: function () {
            alert('Ошибка');
        },
        complete: function () {
            stopLoader();
        }
    })
});
$(document).on('click', '.del-cart', function () {

    var id = $(this).data('id');
    $.ajax({
        url: '/product/del-cart',
        data: {id: id},
        type: 'GET',
        beforeSend: function () {
            runLoader();
        },
        success: function () {
            location.reload();
        },
        error: function () {
            alert('Ошибка');
        },
        complete: function () {
            stopLoader();
        }
    })
});

///Model

$('.myall_modal_in').on('shown.bs.modal', function (e) {
    $('.cons_top_slider1').slick('setPosition');
    $('.all4_slider_bot').slick('setPosition');
});


//===============================================================================

//bunyod qoshgan

$(document).on('submit', '#get-code', function (e) {
    e.preventDefault();
    var data = $(this).serialize();
    var url = $(this).attr('action');
    var type = $(this).attr('method');

    $.ajax({
        type: type,
        url: url,
        data: data,
        success: function (res) {
            if (res == 1) {
                $('.check-code').removeAttr('disabled');
                $('#code').focus();
            } else if (res == 2) {
                swal("Телефон номер не сушествует", "Попробуйте сново!", "warning");
                //alert('Телефон номер не сушествует');
            } else if (res == 3) {
                swal("Не правильный код", "Проверьте и введите правильный код", "error");
                // alert('код не правильный');
            }
            else {
                $('#password-reset').html(res);
            }
        },
        error: function (e) {
            console.log(e);
        }
    });
});

$(document).on('submit', '#order-address', function (e) {
    e.preventDefault();
    var data = $(this).serialize();
    var url = $(this).attr('action');
    var type = $(this).attr('method');

    $.ajax({
        type: type,
        url: url,
        data: data,
        success: function (res) {
            $('#order_id').attr('value', res);
            $('#order_id_2').attr('value',res);
            $('#order_id_3').attr('value',res);
            $('#collapse1').removeClass('in')
            $('#collapse2').addClass('in');
            $('html, body').animate({ scrollTop: $('#collapse2').offset().top }, 'slow');

        },
        error: function (e) {
            console.log(e);
        }
    });
});

$(document).on('click', '.ordering_prev2',function (e) {
    e.preventDefault();
    $('#collapse2').removeClass('in');
    $('#collapse4').removeClass('in');
    $('#collapse1').addClass('in');
    $('html, body').animate({ scrollTop: $('#collapse1').offset().top }, 'slow');
});

$(document).ready(function () {
    var value = $('#code').val();
    if (value == '') {
        $('.check-code').attr('disabled', true);
    }
});

$(document).on('submit', '#order-time', function (e) {
    e.preventDefault();
    var data = $(this).serialize();
    var url = $(this).attr('action');
    var type = $(this).attr('method');

    $.ajax({
        type: type,
        url: url,
        data: data,
        success: function (res) {

            $('#collapse2').removeClass('in')
            $('#collapse3').addClass('in');
            $('html, body').animate({ scrollTop: $('#collapse3').offset().top }, 'slow');

        },
        error: function (e) {
            console.log(e);
        }
    });
});
$(document).on('click', '.ordering_prev3',function (e) {
    e.preventDefault();
    $('#collapse3').removeClass('in');
    $('#collapse4').removeClass('in');
    $('#collapse2').addClass('in');
    $('html, body').animate({ scrollTop: $('#collapse2').offset().top }, 'slow');
});
$(document).on('click', '.ordering_prev4',function (e) {
    e.preventDefault();
    $('#collapse4').removeClass('in');
    $('#collapse3').addClass('in');
    $('html, body').animate({ scrollTop: $('#collapse3').offset().top }, 'slow');
});

$(document).on('submit', '#order-payment', function (e) {
    e.preventDefault();
    var data = $(this).serialize();
    var url = $(this).attr('action');
    var type = $(this).attr('method');

    $.ajax({
        type: type,
        url: url,
        data: data,
        success: function (res) {

            $('#collapse3').removeClass('in')
            $('#collapse4').addClass('in');
            $('html, body').animate({ scrollTop: $('#collapse4').offset().top }, 'slow');
            $('.ordering_content4').html(res);

        },
        error: function (e) {
            console.log(e);
        }
    });
});

$(document).on('keyup', '#code', function (e) {
    e.preventDefault();
    var value = $(this).val();
    if (value == '') {
        $('.check-code').attr('disabled', true);
    } else {
        $('.check-code').attr('disabled', false);
    }
});

$(document).on('click', '.send-code-again', function (e) {
    e.preventDefault();
    var phone = $('#phone').val();
    var url = $(this).data('url');
    $.ajax({
        type: 'get',
        url: url,
        data: {phone: phone},
        success: function (res) {
            $('.1get-code').submit();
        },
        error: function (e) {
            console.log(e);
        }
    })
});
$(document).on('click', '.get-code', function (e) {
    e.preventDefault();
    var url = $(this).data('url');
    var type = 'post';
    var phone = $('#phone').val();
    var csrf = $('#get-code').children('input[name=_csrf]').val();

    if (phone == '') {
        $('#phone').attr('style', 'border-color: red;');
    } else {
        $('#phone').removeAttr('style');
        $.ajax({
            type: type,
            url: url,
            data: {phone: phone, _csrf: csrf},
            success: function (res) {
                if (res == 2) {
                    alert('Тел номер нет в базе');
                } else {
                    alert(res);
                }
            },
            error: function (e) {
                console.log(e);
            },
        });
    }
});

$(document).on('submit', '#sign-up', function (e) {

    e.preventDefault();
    var data = $(this).serialize();
    var url = $(this).attr('action');
    var type = $(this).attr('method');
    var password = $(this).children('input[name=password]');
    var confirmPassword = $(this).children('input[name=confirmPassword]');

    if (password == confirmPassword) {
        $(this).children('input[name=password]').removeAttr('style');
        $(this).children('input[name=confirmPassword]').removeAttr('style');
        $.ajax({
            type: type,
            url: url,
            data: data,
            success: function (res) {
                $('.registration_ek-all').html(res);
            },
            error: function (e) {
                console.log(e);
            }
        });
    } else {
        $(this).children('input[name=password]').attr('style', 'border-color: red;');
        $(this).children('input[name=confirmPassword]').attr('style', 'border-color: red;');
    }


});

$(document).on('submit', '.set-password-btn', function () {
    // e.preventDefault();
    var pass = $('.set-password');
    var passcom = $('.set-password-com');
    var form = $('#set-pass');
    var url = form.attr('action');
    var type = form.attr('method');
    if (pass != passcom) {
        swal("Пароли не совпадают", "Попробуйте сново!", "warning");
    } else {
        $.ajax({
            type: type,
            url: url,
            data: data,
            success: function (res) {

            },
            error: function (e) {
                console.log('error');
            }
        });
    }
});

$(document).on('change', '.region', function (e) {
    e.preventDefault();
    var region_id = $(this).val();

    $.ajax({
        type: 'GET',
        url: '/site/region',
        data: {region_id: region_id},
        success: function (res) {
            $('.rayon').html(res);


        },
        error: function (e) {
            console.log('error');
        }
    });
});

$(document).on('submit', '#get-code-new', function (e) {
    e.preventDefault();
    var data = $(this).serialize();
    var url = $(this).attr('action');
    var type = $(this).attr('method');

    $.ajax({
        type: type,
        url: url,
        data: data,
        success: function (res) {

            $('#code').removeAttr('disabled');
            $('#code').attr('required', 'required');
            $('#code').focus();
            $('#order_id').attr('value', res);
            $('#order_id_2').attr('value',res);
            $('#order_id_3').attr('value',res);
        },
        error: function (e) {
            console.log(e);
        }
    });
});
var password = document.getElementById("password")
    , confirm_password = document.getElementById("confirm_password");

function validatePassword(){
    if(password.value != confirm_password.value) {
        confirm_password.setCustomValidity("Пароли не совпадают");
    } else {
        confirm_password.setCustomValidity('');
    }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;

$(document).on('change', '#code', function () {
    var phone = $('#get-code-new').find('input[name=phone]').val();
    var code = $('#code').val();

    $.ajax({
        type: 'get',
        url: '/product/check-code-phone',
        data: {code: code, phone: phone},
        success: function (res) {
            if (res == 1) {
                $('#code').attr('style', 'border-color: blue;');
                $('.ordering_next1').removeAttr('disabled');
            }
            if (res == 2) {
                swal("Не правильный код", "Проверьте и введите правильный код", "error");
            }
        },
        error: function (e) {
            swal("Не правильный код", "Проверьте и введите правильный код", "error");
        }
    })

});
$(docment).on('click', '.send-code-again-new', function (e) {
    e.preventDefault();
    $('#get-code-new').submit();

});




//===============================================================================