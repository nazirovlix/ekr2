<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pages".
 *
 * @property int $id
 * @property string $path
 * @property string $title_ru
 * @property string $title_uz
 * @property string $content_ru
 * @property string $content_uz
 * @property string $text1_ru
 * @property string $text1_uz
 * @property string $text2_ru
 * @property string $text2_uz
 */
class Pages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pages';
    }
    public function behaviors()
    {
        return [
            [
                'class' => '\app\components\FileUploadBehaviour'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title_ru', 'title_uz', 'content_ru', 'content_uz', 'text1_ru', 'text1_uz', 'text2_ru', 'text2_uz'], 'required'],
            [['content_ru', 'content_uz'], 'string'],
            [['title_ru', 'title_uz', 'text1_ru', 'text1_uz', 'text2_ru', 'text2_uz'], 'string', 'max' => 255],
            [['path'],'file'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'path' => Yii::t('app', 'Image'),
            'title_ru' => Yii::t('app', 'Title Ru'),
            'title_uz' => Yii::t('app', 'Title Uz'),
            'content_ru' => Yii::t('app', 'Content Ru'),
            'content_uz' => Yii::t('app', 'Content Uz'),
            'text1_ru' => Yii::t('app', 'Text1 Ru'),
            'text1_uz' => Yii::t('app', 'Text1 Uz'),
            'text2_ru' => Yii::t('app', 'Text2 Ru'),
            'text2_uz' => Yii::t('app', 'Text2 Uz'),
        ];
    }
    public function getTitle(){

        if (Yii::$app->language == 'ru'):  return $this->title_ru;

        endif;
        if (Yii::$app->language == 'uz'):  return $this->title_uz;

        endif;
    }

    public function getContent(){

        if (Yii::$app->language == 'ru'):  return $this->content_ru;

        endif;
        if (Yii::$app->language == 'uz'):  return $this->content_uz;

        endif;
    }
    public function getText1(){

        if (Yii::$app->language == 'ru'):  return $this->text1_ru;

        endif;
        if (Yii::$app->language == 'uz'):  return $this->text1_uz;

        endif;
    }
    public function getText2(){

        if (Yii::$app->language == 'ru'):  return $this->text2_ru;

        endif;
        if (Yii::$app->language == 'uz'):  return $this->text2_uz;

        endif;
    }
}
