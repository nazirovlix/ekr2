<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sms".
 *
 * @property int $id
 * @property string $phone
 * @property string $sms
 * @property int $try_count
 * @property int $send_at
 */
class Sms extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sms';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['try_count', 'send_at'], 'integer'],
            [['phone', 'sms'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'phone' => Yii::t('app', 'Phone'),
            'sms' => Yii::t('app', 'Sms'),
            'try_count' => Yii::t('app', 'Try Count'),
            'send_at' => Yii::t('app', 'Send At'),
        ];
    }
}
