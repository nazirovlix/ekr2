<?php

namespace app\models;

use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Exception;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property int $code
 * @property string $base_url
 * @property string $path
 * @property string $title_ru
 * @property string $title_uz
 * @property string $content_ru
 * @property string $content_uz
 * @property int $category_id
 * @property int $price
 * @property int $discount_price
 * @property int $discount
 * @property int $country_id
 * @property int $brand_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $status
 */
class Products extends \yii\db\ActiveRecord
{

    public $attachments;
    public $file;

    public $sizes;
    public $size_id;
    public $colors;
    public $color_name;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products';
    }

    public function afterSave($insert, $changedAttributes)
    {


        if (is_array($this->size_id)) {
            if (!$this->isNewRecord) {
                ProductSize::deleteAll(['product_id' => $this->id]);
            }
            foreach ($this->size_id as $item) {
                try {
                    Yii::$app->db->createCommand()->insert('product_size', [
                        'product_id' => $this->id,
                        'size_id' => $item,
                    ])->execute();
                } catch (Exception $e) {
                    echo $e->getMessage();
                }
            }
        }
        if (is_array($this->color_name)) {
            if (!$this->isNewRecord) {
                ProductColors::deleteAll(['product_id' => $this->id]);
            }
            foreach ($this->color_name as $item) {
                try {
                    Yii::$app->db->createCommand()->insert('product_colors', [
                        'product_id' => $this->id,
                        'color_id' => $item,
                    ])->execute();
                } catch (Exception $e) {
                    echo $e->getMessage();
                }
            }
        }
        parent::afterSave($insert, $changedAttributes);
    }

    public function afterFind()

    {

        $sizes = ProductSize::find()->where(['product_id' => $this->id])->all();
        $j = 0;
        foreach ($sizes as $item):
            $this->sizes[$j]['size_id'] = $item->size_id;
            $j++;
        endforeach;

        $colors = ProductColors::find()->where(['product_id' => $this->id])->all();
        $k = 0;
        foreach ($colors as $item):
            $this->colors[$k]['color_name'] = $item->color_id;
            $k++;
        endforeach;

        return parent::afterFind();
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'file',
                'pathAttribute' => 'path',
                'baseUrlAttribute' => 'base_url',
            ],
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'attachments',
                'multiple' => true,
                'uploadRelation' => 'productImages',
                'pathAttribute' => 'path',
                'baseUrlAttribute' => false,
            ],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code', 'category_id', 'price', 'discount_price', 'discount', 'country_id', 'brand_id', 'created_at', 'updated_at', 'status'], 'integer'],
            [['title_ru', 'title_uz', 'content_ru', 'content_uz', 'category_id'], 'required'],
            [['content_ru', 'content_uz'], 'string'],
            [['base_url', 'path', 'title_ru', 'title_uz'], 'string', 'max' => 255],
            [['attachments', 'file', 'sizes', 'size_id', 'colors', 'color_name'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'code' => Yii::t('app', 'Code'),
            'base_url' => Yii::t('app', 'Base Url'),
            'path' => Yii::t('app', 'Path'),
            'title_ru' => Yii::t('app', 'Title Ru'),
            'title_uz' => Yii::t('app', 'Title Uz'),
            'content_ru' => Yii::t('app', 'Content Ru'),
            'content_uz' => Yii::t('app', 'Content Uz'),
            'category_id' => Yii::t('app', 'Category ID'),
            'price' => Yii::t('app', 'Price'),
            'discount_price' => Yii::t('app', 'Discount Price'),
            'discount' => Yii::t('app', 'Discount'),
            'country_id' => Yii::t('app', 'Country ID'),
            'brand_id' => Yii::t('app', 'Brand ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'status' => Yii::t('app', 'Status'),
            'file' => Yii::t('app', 'Main Image'),
            'attachments' => Yii::t('app', 'Product Images'),
            'size_id' => Yii::t('app', 'Sizes'),
            'sizes' => Yii::t('app', 'Sizes'),
        ];
    }

    public function getProductImages()
    {
        return $this->hasMany(ProductImages::className(), ['product_id' => 'id']);
    }

    public function getTitle()
    {

        if (Yii::$app->language == 'ru'): return $this->title_ru;

        endif;
        if (Yii::$app->language == 'uz'): return $this->title_uz;

        endif;
    }

    public function getContent()
    {

        if (Yii::$app->language == 'ru'): return $this->content_ru;

        endif;
        if (Yii::$app->language == 'uz'): return $this->content_uz;

        endif;
    }

    function getColorss()
    {

        return $this->hasMany(ProductColors::className(), ['product_id' => 'id']);
    }

    function getSizess()
    {

        return $this->hasMany(ProductSize::className(), ['product_id' => 'id']);
    }
    function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    function getPimages()
    {

        return $this->hasMany(ProductImages::className(), ['product_id' => 'id']);
    }

    public function addToCart($product, $color, $size)
    {
        $qty = 1;
        if ($product->discount_price == 0) {
            $price = $product->price;
        } else {
            $price = $product->discount_price;
        }

        $session = Yii::$app->session['cart'];
        if(!empty($session)){
            $session = $_SESSION['cart'];
            $a = 0;
            foreach ($session as $k => $item){
                if(($item['id'] == $product->id) && ($item['color'] == $color) && ($item['size'] == $size)){
                    $a = 1;
                    $key = $k;
                }
            }
            if($a == 1){
                $_SESSION['cart'][$key]['qty'] += $qty;
            } else{
                $_SESSION['cart'][] = [
                    'id' => $product->id,
                    'qty' => $qty,
                    'color' => $color,
                    'size' => $size,
                    'name' => $product->title,
                    'price' => $price,
                    'code' => $product->code,
                    'img' => $product->path,
                ];
            }

        } else {
            $_SESSION['cart'][] = [
                'id' => $product->id,
                'qty' => $qty,
                'color' => $color,
                'size' => $size,
                'name' => $product->title,
                'price' => $price,
                'code' => $product->code,
                'img' => $product->path,
            ];
        }
        $_SESSION['cart.qty'] = isset($_SESSION['cart.qty']) ? $_SESSION['cart.qty'] + $qty : $qty;
        $_SESSION['cart.sum'] = isset($_SESSION['cart.sum']) ? $_SESSION['cart.sum'] + $qty * $price : $qty * $price;
        return true;
    }


    public function recalc($id){
        $price = $_SESSION['cart'][$id]['price'];
        $qty = $_SESSION['cart'][$id]['qty'];
        $_SESSION['cart.qty'] -= $qty;
        $_SESSION['cart.sum'] -= $qty * $price;
        unset($_SESSION['cart'][$id]);
    }



}
