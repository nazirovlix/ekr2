<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_color".
 *
 * @property int $id
 * @property string $color_name_ru
 * @property string $color_name_uz
 * @property string $code
 */
class ProductColor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_color';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['color_name_ru', 'color_name_uz', 'code'], 'required'],
            [['color_name_ru', 'color_name_uz', 'code'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'color_name_ru' => Yii::t('app', 'Color Name Ru'),
            'color_name_uz' => Yii::t('app', 'Color Name Uz'),
            'code' => Yii::t('app', 'Code'),
        ];
    }

    public function getColors() {
        return $this->hasMany(ProductColor::className(), ['product_id' => 'id'] );
    }


    public function getTitle(){

        if (Yii::$app->language == 'ru'):  return $this->color_name_ru;

        endif;
        if (Yii::$app->language == 'uz'):  return $this->color_name_uz;

        endif;
    }

}
