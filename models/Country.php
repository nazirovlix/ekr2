<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "country".
 *
 * @property int $id
 * @property string $country_ru
 * @property string $country_uz
 * @property int $status
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['country_ru', 'country_uz'], 'required'],
            [['status'], 'integer'],
            [['country_ru', 'country_uz'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'country_ru' => Yii::t('app', 'Country Ru'),
            'country_uz' => Yii::t('app', 'Country Uz'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    public function getTitle(){

        if (Yii::$app->language == 'ru'):  return $this->country_ru;

        endif;
        if (Yii::$app->language == 'uz'):  return $this->country_uz;

        endif;
    }
}
