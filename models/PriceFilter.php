<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "price_filter".
 *
 * @property int $id
 * @property int $from
 * @property int $to
 * @property int $status
 */
class PriceFilter extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'price_filter';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['from', 'to', 'status'], 'integer'],
            [['status'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'from' => Yii::t('app', 'From'),
            'to' => Yii::t('app', 'To'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
}
